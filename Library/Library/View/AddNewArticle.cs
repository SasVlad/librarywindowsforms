﻿using Library.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Model;
using Library.Model.ViewModel;

namespace Library.View
{
    public partial class AddNewArticle : Form, IAddNewArticlePresentation
    {
        public event Action AddArticle;
        public event Action AddAuthor;
        public event Action FindArticle;

        private List<AuthorModel> _allAuthors;
        private List<AuthorModel> _allArticleAuthors = new List<AuthorModel>();
        private bool _flagChageTable = true;
        List<string> listAuthors = new List<string>();

        public AddNewArticle()
        {
            InitializeComponent();

            BtnAddArticle.Click += (sender, args) =>
            {
                
                var _condition = tbBody.Text != string.Empty && tbBody.Text != string.Empty && tbTitle.Text != string.Empty&& _allArticleAuthors.Count!=0;

                MyCustomCondition.ChooseCondition(
                    _condition,
                    ()=> 
                    {
                        _flagChageTable = true;
                        Invoke(AddArticle);
                        this.Close();
                    },
                    "Input correct data or input not all fields");

            };

            this.Load += (sender, args) => Invoke(FindArticle);

            btnAddNewAuthor.Click += (sender, args) => {
                var _condition = tbAuthorFirstName.Text != string.Empty && tbAuthorLastName.Text != string.Empty;
                MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        _flagChageTable = true;
                        Invoke(AddAuthor);
                    },
                    "Input correct data or input not all fields");
            };

        }


        public AuthorModel GetAuthorToAdd
        {
            get
            {
                return new AuthorModel {FirstName=tbAuthorFirstName.Text,LastName=tbAuthorLastName.Text };

            }

        }

        public ArticleViewModel GetArticleToAdd
        {
            get
            {
                return new ArticleViewModel
                {
                    Article = new ArticleModel { Body=tbBody.Text,Image=tbBody.Text,Title=tbTitle.Text },
                    Authors = _allArticleAuthors
                };
            }

        }


        public List<AuthorModel> GetAllAuthors
        {
            get
            {
                return _allAuthors;
            }
            set
            {
                if (_flagChageTable && value.Count != 0)
                {
                    _allAuthors = value;
                    dataGridAllAuthors.DataSource = value;
                    this.dataGridAllAuthors.CurrentCell = null;
                    _flagChageTable = false;
                    dataGridAllAuthors.Columns["Id"].Visible = false;
                }
            }
        }



        private void Invoke(Action action)
        {
            if (action != null) action();
        }
        public new void Show()
        {
            this.ShowDialog();
        }
 

        private void btnSelectAuthor_Click(object sender, EventArgs e)
        {
            
            if (_allArticleAuthors == null)
            {
                _allArticleAuthors = new List<AuthorModel>();
            }


            var _condition = dataGridAllAuthors.CurrentRow != null;
            MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        dataGridAuthorsCurrentArticle.ClearSelection();
                        _allArticleAuthors.Add((AuthorModel)dataGridAllAuthors.CurrentRow.DataBoundItem);

                        dataGridAuthorsCurrentArticle.DataSource = null;
                        dataGridAuthorsCurrentArticle.DataSource = _allArticleAuthors;
                        dataGridAuthorsCurrentArticle.Columns["Id"].Visible = false;
                    },
                    "Choose author");        

        }


    }
}
