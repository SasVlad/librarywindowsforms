﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library.View
{
    public partial class AddNewJournal : Form, IAddNewJournalPresentation
    {
        public event Action AddJournal;
        public event Action GetFreeArticles;

        private List<ArticleModel> _allArticles;
        private List<ArticleModel> _allJournalArticles = new List<ArticleModel>();
        private bool _flagChageTable = true;
        List<string> listArticles = new List<string>();

        

        public AddNewJournal()
        {
            InitializeComponent();

            BtnAddJournal.Click += (sender, args) =>
            {
                var _condition = @tbCountPages.Text != string.Empty && tbCover.Text != string.Empty
                   && tbEdition.Text != string.Empty && tbName.Text != string.Empty && tbPublisher.Text != string.Empty &&
                   tbType.Text != string.Empty && tbGenre.Text != string.Empty && _allJournalArticles.Count != 0;

                MyCustomCondition.ChooseCondition(
                                _condition,
                                () =>
                                    {
                                    _flagChageTable = true;
                                    Invoke(AddJournal);
                                        this.Close();
                                    },
                        "Input correct data or input not all fields");

            };
            this.Load += (sender, args) => Invoke(GetFreeArticles);
        }



        public JournalViewModel GetJournalToAdd
        {
            get
            {               
                return new JournalViewModel
                {
                    Journal = new JournalModel {CountPages=Convert.ToInt32(tbCountPages.Text),Cover=tbCover.Text,Edition=Convert.ToInt32(tbEdition.Text),Genre=tbGenre.Text,
                    Name=tbName.Text,Publisher=tbPublisher.Text,Type=tbType.Text},
                    Articles = _allJournalArticles
                };
            }

        }
        public List<ArticleModel> GetAllArticle
        {
            get
            {
                return _allArticles;
            }
            set
            {
                if (_flagChageTable)
                {
                    _allArticles = value;
                    dataGridAllArticles.DataSource = value;
                    this.dataGridAllArticles.CurrentCell = null;
                    dataGridAllArticles.Columns["Id"].Visible = false;

                    _flagChageTable = false;
                }
            }
        }


        private void Invoke(Action action)
        {
            if (action != null) action();
        }
        public new void Show()
        {
            this.ShowDialog();
        }


        private void btnSelectArticle_Click(object sender, EventArgs e)
        {
            if (_allJournalArticles == null)
            {
                _allJournalArticles = new List<ArticleModel>();
            }

            var _condition = dataGridAllArticles.CurrentRow != null;
            MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        dataGridArticlesCurrentJournal.ClearSelection();
                        _allJournalArticles.Add((ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem);

                        dataGridArticlesCurrentJournal.DataSource = null;
                        dataGridArticlesCurrentJournal.DataSource = _allJournalArticles;
                        if (dataGridArticlesCurrentJournal.RowCount != 0)
                        {
                            dataGridArticlesCurrentJournal.Columns["Id"].Visible = false;
                        }
                    },
                    "Choose article");
            
        }

    }
}
