﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using Library.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library.View
{
    public partial class LibraryArticleForm : Form, ILibraryArticleView
    {
        public event Action FindArticle;
        public event Action RemoveArticle;
        public event Action UpdateArticle;
        public event Action DeleteAuthorArticle;

        private List<ArticleViewModel> _allArticles;
        private List<AuthorModel> _allAuthors;
        private List<AuthorModel> _allArticleAuthors = new List<AuthorModel>();
        private bool _flagChageTable = true;
        List<string> listAuthors = new List<string>();
        public LibraryArticleForm()
        {
            InitializeComponent();

             BtnFindArticle.Click += (sender, args) => Invoke(FindArticle);
            BtnDeleteArticle.Click += (sender, args) => Invoke(RemoveArticle);

            BtnUpdate.Click += (sender, args) => Invoke(UpdateArticle);
            this.Load += (sender, args) => Invoke(FindArticle);

            dataGridArticles.SelectionChanged += ChangeIndexInDatagridArticle_Event;
            btnDeleteAuthorArticle.Click += (sender, args) => Invoke(DeleteAuthorArticle);
        }

        public ArticleViewModel GetArticle
        {
            get
            {
                listAuthors.Clear();
                var _listAuthors = dataGridAllAuthors.CurrentCell == null ? new List<AuthorModel>() : new List<AuthorModel>() {
                    (AuthorModel)dataGridAllAuthors.CurrentRow.DataBoundItem };
                return new ArticleViewModel
                {
                    Article = new ArticleModel { Title = tbTitleArticle.Text },
                    Authors = _listAuthors

                };
            }

        }
        public AuthorModel GetAuthorToAdd
        {
            get
            {
                return dataGridAllAuthors.CurrentCell == null ? new AuthorModel() : (AuthorModel)dataGridAllAuthors.CurrentRow.DataBoundItem;

            }

        }
        public AuthorModel GetCurrentAuthorArticle
        {
            get
            {
                return dataGridAuthorsCurrentArticle.CurrentCell == null ? new AuthorModel() : (AuthorModel)dataGridAuthorsCurrentArticle.CurrentRow.DataBoundItem;

            }

        }

        public int ArticleDataGridIndex
        {
            get
            {
                return this.dataGridArticles.CurrentRow.Index;
            }
        }

        public List<AuthorModel> GetAllAuthors
        {
            get
            {
                return _allAuthors;
            }
            set
            {
                if (_flagChageTable)
                {
                    _allAuthors = value;
                    dataGridAllAuthors.DataSource = value;
                    this.dataGridAllAuthors.CurrentCell = null;
                    _flagChageTable = false;
                    dataGridAllAuthors.Columns["Id"].Visible = false;
                }
            }
        }
        private void FillDataSource(List<ArticleModel> listArticles, List<AuthorModel> listAuthorModel)
        {
            dataGridAuthorsCurrentArticle.DataSource = listAuthorModel;

            dataGridArticles.DataSource = listArticles;
            dataGridArticles.Columns["Id"].Visible = false;
            dataGridAuthorsCurrentArticle.Columns["Id"].Visible = false;
            this.dataGridAllAuthors.CurrentCell = null;
        }
        public List<ArticleViewModel> LibraryArticles
        {
            get
            {
                return _allArticles;
            }
            set
            {

                dataGridArticles.ClearSelection();
                _allArticles = value;
                var _listArticles = new List<ArticleModel>();

                value.ForEach(x => {
                    _listArticles.Add(x.Article);
                });
                
                if (value.Count != 0)
                {
                    FillDataSource(_listArticles, value[0].Authors);
                }
                if (value.Count == 0)
                {
                    FillDataSource(new List<ArticleModel>(), new List<AuthorModel>());
                }

            }
        }
        public ArticleModel CurrentArticle
        {
            get
            {
                if (dataGridArticles.Rows.Count != 0)
                {
                    return (ArticleModel)dataGridArticles.CurrentRow.DataBoundItem;
                }

                MessageBox.Show("Data Grid is empty");
                return null;


            }
        }

        public ArticleViewModel GetArticleToDelete
        {
            get
            {
                listAuthors.Clear();
                var _Article = dataGridArticles.CurrentCell == null ? new ArticleModel() : (ArticleModel)dataGridArticles.CurrentRow.DataBoundItem;
                return new ArticleViewModel
                {
                    Article = _Article,
                    Authors = _allArticleAuthors
                };
            }
        }

        private void Invoke(Action action)
        {
            if (action != null) action();
        }

        //--------------------chaged event
        private void ChangeIndexInDatagridArticle_Event(object sender, EventArgs e)
        {
            if (this.dataGridArticles.CurrentRow != null && _allArticles != null)
            {
                dataGridAuthorsCurrentArticle.DataSource = _allArticles[ArticleDataGridIndex].Authors;
            }
        }

        private void btnNewArticle_Click(object sender, EventArgs e)
        {
            new AddNewArticlePresentation(new AddNewArticle()).Run();
            Invoke(FindArticle);
        }


        private void btnBooksLibrary_Click(object sender, EventArgs e)
        {

        }
    }
}
