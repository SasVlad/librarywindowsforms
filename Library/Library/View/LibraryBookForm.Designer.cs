﻿namespace Library
{
    partial class LibraryBookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridBooks = new System.Windows.Forms.DataGridView();
            this.BtnDeleteBook = new System.Windows.Forms.Button();
            this.BtnFindBook = new System.Windows.Forms.Button();
            this.tbNameBook = new System.Windows.Forms.TextBox();
            this.lbNameBook = new System.Windows.Forms.Label();
            this.lbAuthor = new System.Windows.Forms.Label();
            this.dataGridAllAuthors = new System.Windows.Forms.DataGridView();
            this.dataGridAuthorsCurrentBook = new System.Windows.Forms.DataGridView();
            this.lbAuthorsCurrentBook = new System.Windows.Forms.Label();
            this.btnNewBook = new System.Windows.Forms.Button();
            this.btnDeleteAuthorArticle = new System.Windows.Forms.Button();
            this.BtnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentBook)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridBooks
            // 
            this.dataGridBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBooks.Location = new System.Drawing.Point(12, 12);
            this.dataGridBooks.Name = "dataGridBooks";
            this.dataGridBooks.Size = new System.Drawing.Size(555, 282);
            this.dataGridBooks.TabIndex = 0;
            // 
            // BtnDeleteBook
            // 
            this.BtnDeleteBook.Location = new System.Drawing.Point(305, 401);
            this.BtnDeleteBook.Name = "BtnDeleteBook";
            this.BtnDeleteBook.Size = new System.Drawing.Size(102, 28);
            this.BtnDeleteBook.TabIndex = 2;
            this.BtnDeleteBook.Text = "DeleteBook";
            this.BtnDeleteBook.UseVisualStyleBackColor = true;
            // 
            // BtnFindBook
            // 
            this.BtnFindBook.Location = new System.Drawing.Point(576, 352);
            this.BtnFindBook.Name = "BtnFindBook";
            this.BtnFindBook.Size = new System.Drawing.Size(241, 43);
            this.BtnFindBook.TabIndex = 3;
            this.BtnFindBook.Text = "FindBook";
            this.BtnFindBook.UseVisualStyleBackColor = true;
            // 
            // tbNameBook
            // 
            this.tbNameBook.Location = new System.Drawing.Point(646, 311);
            this.tbNameBook.Name = "tbNameBook";
            this.tbNameBook.Size = new System.Drawing.Size(171, 20);
            this.tbNameBook.TabIndex = 4;
            // 
            // lbNameBook
            // 
            this.lbNameBook.AutoSize = true;
            this.lbNameBook.Location = new System.Drawing.Point(577, 318);
            this.lbNameBook.Name = "lbNameBook";
            this.lbNameBook.Size = new System.Drawing.Size(63, 13);
            this.lbNameBook.TabIndex = 7;
            this.lbNameBook.Text = "Name Book";
            // 
            // lbAuthor
            // 
            this.lbAuthor.AutoSize = true;
            this.lbAuthor.Location = new System.Drawing.Point(573, 9);
            this.lbAuthor.Name = "lbAuthor";
            this.lbAuthor.Size = new System.Drawing.Size(57, 13);
            this.lbAuthor.TabIndex = 8;
            this.lbAuthor.Text = "All Authors";
            // 
            // dataGridAllAuthors
            // 
            this.dataGridAllAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllAuthors.Location = new System.Drawing.Point(576, 25);
            this.dataGridAllAuthors.Name = "dataGridAllAuthors";
            this.dataGridAllAuthors.Size = new System.Drawing.Size(368, 233);
            this.dataGridAllAuthors.TabIndex = 17;
            // 
            // dataGridAuthorsCurrentBook
            // 
            this.dataGridAuthorsCurrentBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAuthorsCurrentBook.Location = new System.Drawing.Point(12, 315);
            this.dataGridAuthorsCurrentBook.Name = "dataGridAuthorsCurrentBook";
            this.dataGridAuthorsCurrentBook.Size = new System.Drawing.Size(555, 80);
            this.dataGridAuthorsCurrentBook.TabIndex = 18;
            // 
            // lbAuthorsCurrentBook
            // 
            this.lbAuthorsCurrentBook.AutoSize = true;
            this.lbAuthorsCurrentBook.Location = new System.Drawing.Point(24, 301);
            this.lbAuthorsCurrentBook.Name = "lbAuthorsCurrentBook";
            this.lbAuthorsCurrentBook.Size = new System.Drawing.Size(116, 13);
            this.lbAuthorsCurrentBook.TabIndex = 19;
            this.lbAuthorsCurrentBook.Text = "Authors Selected Book";
            // 
            // btnNewBook
            // 
            this.btnNewBook.Location = new System.Drawing.Point(60, 401);
            this.btnNewBook.Name = "btnNewBook";
            this.btnNewBook.Size = new System.Drawing.Size(116, 28);
            this.btnNewBook.TabIndex = 20;
            this.btnNewBook.Text = "Add New Book";
            this.btnNewBook.UseVisualStyleBackColor = true;
            this.btnNewBook.Click += new System.EventHandler(this.btnNewBook_Click);
            // 
            // btnDeleteAuthorArticle
            // 
            this.btnDeleteAuthorArticle.Location = new System.Drawing.Point(413, 401);
            this.btnDeleteAuthorArticle.Name = "btnDeleteAuthorArticle";
            this.btnDeleteAuthorArticle.Size = new System.Drawing.Size(125, 28);
            this.btnDeleteAuthorArticle.TabIndex = 23;
            this.btnDeleteAuthorArticle.Text = "DeleteAuthor";
            this.btnDeleteAuthorArticle.UseVisualStyleBackColor = true;
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.Location = new System.Drawing.Point(182, 401);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(117, 28);
            this.BtnUpdate.TabIndex = 11;
            this.BtnUpdate.Text = "UpdateBook";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            // 
            // LibraryBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 443);
            this.Controls.Add(this.btnDeleteAuthorArticle);
            this.Controls.Add(this.btnNewBook);
            this.Controls.Add(this.lbAuthorsCurrentBook);
            this.Controls.Add(this.dataGridAuthorsCurrentBook);
            this.Controls.Add(this.dataGridAllAuthors);
            this.Controls.Add(this.BtnUpdate);
            this.Controls.Add(this.lbAuthor);
            this.Controls.Add(this.lbNameBook);
            this.Controls.Add(this.tbNameBook);
            this.Controls.Add(this.BtnFindBook);
            this.Controls.Add(this.BtnDeleteBook);
            this.Controls.Add(this.dataGridBooks);
            this.Name = "LibraryBookForm";
            this.Text = "Library";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentBook)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridBooks;
        private System.Windows.Forms.Button BtnDeleteBook;
        private System.Windows.Forms.Button BtnFindBook;
        private System.Windows.Forms.TextBox tbNameBook;
        private System.Windows.Forms.Label lbNameBook;
        private System.Windows.Forms.Label lbAuthor;
        private System.Windows.Forms.DataGridView dataGridAllAuthors;
        private System.Windows.Forms.DataGridView dataGridAuthorsCurrentBook;
        private System.Windows.Forms.Label lbAuthorsCurrentBook;
        private System.Windows.Forms.Button btnNewBook;
        private System.Windows.Forms.Button btnDeleteAuthorArticle;
        private System.Windows.Forms.Button BtnUpdate;
    }
}

