﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using Library.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library.View
{
    public partial class LibraryNewspaperForm : Form, ILibraryNewspaperView
    {
        public event Action FindNewspaper;
        public event Action RemoveNewspaper;
        public event Action UpdateNewspaper;
        public event Action DeleteArticleNewspaper;
        public event Action GetFreeNewspaperArticles;

        private List<NewspaperViewModel> _allNewspapers;
        private List<ArticleModel> _allArticles;
        private List<ArticleModel> _allNewspaperArticles = new List<ArticleModel>();
        private bool _flagChageTable = true;
        List<string> listArticles = new List<string>();
        public LibraryNewspaperForm()
        {
            InitializeComponent();

            BtnFindNewspaper.Click += (sender, args) => Invoke(FindNewspaper);
            BtnDeleteNewspaper.Click += (sender, args) => { _flagChageTable = true; Invoke(RemoveNewspaper); };
            BtnAddNewspaper.Click += (sender, args) =>
            {
                new AddNewNewspaperPresentation(new AddNewNewspaper()).Run();
                Invoke(FindNewspaper);
                
            };

            BtnUpdateNewspaper.Click += (sender, args) => Invoke(UpdateNewspaper);
            this.Load += (sender, args) => Invoke(FindNewspaper);
            dataGridNewspapers.SelectionChanged += ChangeIndexInDatagridNewspaper_Event;
            btnDeleteArticle.Click += (sender, args) => { _flagChageTable = true; Invoke(DeleteArticleNewspaper); };
        }

        public NewspaperViewModel GetNewspaper
        {
            get
            {
                listArticles.Clear();
                var _listArticles = dataGridAllArticles.CurrentCell == null ? new List<ArticleModel>() : new List<ArticleModel>() {
                    (ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem };
                return new NewspaperViewModel
                {
                    Newspaper = new NewspaperModel { Name = tbNameNewspaper.Text },
                    Articles = _listArticles

                };
            }

        }

        public ArticleModel GetArticleToAdd
        {
            get
            {
                return dataGridAllArticles.CurrentCell == null ? new ArticleModel() : (ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem;

            }

        }
        public ArticleModel GetCurrentArticleNewspaper
        {
            get
            {
                return dataGridArticlesCurrentNewspaper.CurrentCell == null ? new ArticleModel() : (ArticleModel)dataGridArticlesCurrentNewspaper.CurrentRow.DataBoundItem;

            }

        }
        public NewspaperViewModel GetNewspaperToDelete
        {
            get
            {
                listArticles.Clear();
                var _book = dataGridNewspapers.CurrentCell == null ? new NewspaperModel() : (NewspaperModel)dataGridNewspapers.CurrentRow.DataBoundItem;
                return new NewspaperViewModel
                {
                    Newspaper = _book,
                    Articles = _allNewspaperArticles
                };
            }

        }
        public int NewspaperDataGridIndex
        {
            get
            {
                return this.dataGridNewspapers.CurrentRow.Index;
            }
        }

        public List<ArticleModel> GetAllArticle
        {
            get
            {
                return _allArticles;
            }
            set
            {
                if (_flagChageTable)
                {
                    if (value.Count != 0) { 

                        _allArticles = value;
                    dataGridAllArticles.DataSource = value;
                    this.dataGridAllArticles.CurrentCell = null;
                    dataGridAllArticles.Columns["Id"].Visible = false;

                    _flagChageTable = false;
                }
                }
            }
        }
        private void FillDataSource(List<NewspaperModel> listNewspaper, List<ArticleModel> listArtistModel)
        {
            dataGridArticlesCurrentNewspaper.DataSource = listArtistModel;
            dataGridNewspapers.DataSource = listNewspaper;
            dataGridArticlesCurrentNewspaper.Columns["Id"].Visible = false;
            dataGridNewspapers.Columns["Id"].Visible = false;
        }

        public List<NewspaperViewModel> LibraryNewspapers
        {
            get
            {
                return _allNewspapers;
            }
            set
            {

                dataGridNewspapers.ClearSelection();
                _allNewspapers = value;
                var _listNewspapers = new List<NewspaperModel>();

                value.ForEach(x => {
                    _listNewspapers.Add(x.Newspaper);
                });
                if (value.Count != 0)
                {
                    FillDataSource(_listNewspapers,value[0].Articles);
                }
                if (value.Count == 0)
                {
                    FillDataSource(new List<NewspaperModel>(), new List<ArticleModel>());
                }
                    this.dataGridAllArticles.CurrentCell = null;
            }
        }

        public NewspaperModel CurrentNewspaper
        {
            get
            {
                if (dataGridNewspapers.Rows.Count != 0)
                {
                    return (NewspaperModel)dataGridNewspapers.CurrentRow.DataBoundItem;
                }

                MessageBox.Show("Data Grid is empty");
                return null;


            }
        }

        private void Invoke(Action action)
        {
            if (action != null) action();
        }

        //--------------------chaged event
        private void ChangeIndexInDatagridNewspaper_Event(object sender, EventArgs e)
        {
            if (this.dataGridNewspapers.CurrentRow != null && _allNewspapers != null)
            {
                dataGridArticlesCurrentNewspaper.DataSource = _allNewspapers[NewspaperDataGridIndex].Articles;
            }
        }


        private void btnAddArticleToList_Click(object sender, EventArgs e)
        {
            dataGridAllArticles.DataSource = null;
            if (_allArticles == null)
            {
                _allArticles = new List<ArticleModel>();
            }
            if (_allArticles != null)
            {
                _allArticles.Add(new ArticleModel());
            }

            dataGridAllArticles.DataSource = _allArticles;
            if (dataGridAllArticles.ColumnCount!=0)
            {
                dataGridAllArticles.Columns["Id"].Visible = false;
            }
            
        }

    }
}
