﻿namespace Library.View
{
    partial class LibraryNewspaperForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteArticle = new System.Windows.Forms.Button();
            this.lbArticlesCurrentNewspaper = new System.Windows.Forms.Label();
            this.dataGridArticlesCurrentNewspaper = new System.Windows.Forms.DataGridView();
            this.dataGridAllArticles = new System.Windows.Forms.DataGridView();
            this.BtnUpdateNewspaper = new System.Windows.Forms.Button();
            this.lbNameNewspaper = new System.Windows.Forms.Label();
            this.tbNameNewspaper = new System.Windows.Forms.TextBox();
            this.BtnFindNewspaper = new System.Windows.Forms.Button();
            this.BtnDeleteNewspaper = new System.Windows.Forms.Button();
            this.dataGridNewspapers = new System.Windows.Forms.DataGridView();
            this.lbArticles = new System.Windows.Forms.Label();
            this.BtnAddNewspaper = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentNewspaper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNewspapers)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDeleteArticle
            // 
            this.btnDeleteArticle.Location = new System.Drawing.Point(329, 391);
            this.btnDeleteArticle.Name = "btnDeleteArticle";
            this.btnDeleteArticle.Size = new System.Drawing.Size(137, 40);
            this.btnDeleteArticle.TabIndex = 52;
            this.btnDeleteArticle.Text = "DeleteArticle";
            this.btnDeleteArticle.UseVisualStyleBackColor = true;
            // 
            // lbArticlesCurrentNewspaper
            // 
            this.lbArticlesCurrentNewspaper.AutoSize = true;
            this.lbArticlesCurrentNewspaper.Location = new System.Drawing.Point(14, 224);
            this.lbArticlesCurrentNewspaper.Name = "lbArticlesCurrentNewspaper";
            this.lbArticlesCurrentNewspaper.Size = new System.Drawing.Size(143, 13);
            this.lbArticlesCurrentNewspaper.TabIndex = 49;
            this.lbArticlesCurrentNewspaper.Text = "Articles Selected Newspaper";
            // 
            // dataGridArticlesCurrentNewspaper
            // 
            this.dataGridArticlesCurrentNewspaper.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridArticlesCurrentNewspaper.Location = new System.Drawing.Point(14, 240);
            this.dataGridArticlesCurrentNewspaper.Name = "dataGridArticlesCurrentNewspaper";
            this.dataGridArticlesCurrentNewspaper.Size = new System.Drawing.Size(490, 138);
            this.dataGridArticlesCurrentNewspaper.TabIndex = 48;
            // 
            // dataGridAllArticles
            // 
            this.dataGridAllArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllArticles.Location = new System.Drawing.Point(510, 26);
            this.dataGridAllArticles.Name = "dataGridAllArticles";
            this.dataGridAllArticles.Size = new System.Drawing.Size(436, 195);
            this.dataGridAllArticles.TabIndex = 47;
            // 
            // BtnUpdateNewspaper
            // 
            this.BtnUpdateNewspaper.Location = new System.Drawing.Point(198, 384);
            this.BtnUpdateNewspaper.Name = "BtnUpdateNewspaper";
            this.BtnUpdateNewspaper.Size = new System.Drawing.Size(110, 23);
            this.BtnUpdateNewspaper.TabIndex = 46;
            this.BtnUpdateNewspaper.Text = "Update Newspaper";
            this.BtnUpdateNewspaper.UseVisualStyleBackColor = true;
            // 
            // lbNameNewspaper
            // 
            this.lbNameNewspaper.AutoSize = true;
            this.lbNameNewspaper.Location = new System.Drawing.Point(510, 243);
            this.lbNameNewspaper.Name = "lbNameNewspaper";
            this.lbNameNewspaper.Size = new System.Drawing.Size(92, 13);
            this.lbNameNewspaper.TabIndex = 45;
            this.lbNameNewspaper.Text = "Name Newspaper";
            // 
            // tbNameNewspaper
            // 
            this.tbNameNewspaper.Location = new System.Drawing.Point(608, 234);
            this.tbNameNewspaper.Name = "tbNameNewspaper";
            this.tbNameNewspaper.Size = new System.Drawing.Size(222, 20);
            this.tbNameNewspaper.TabIndex = 44;
            // 
            // BtnFindNewspaper
            // 
            this.BtnFindNewspaper.Location = new System.Drawing.Point(564, 275);
            this.BtnFindNewspaper.Name = "BtnFindNewspaper";
            this.BtnFindNewspaper.Size = new System.Drawing.Size(247, 43);
            this.BtnFindNewspaper.TabIndex = 43;
            this.BtnFindNewspaper.Text = "Find Newspaper";
            this.BtnFindNewspaper.UseVisualStyleBackColor = true;
            // 
            // BtnDeleteNewspaper
            // 
            this.BtnDeleteNewspaper.Location = new System.Drawing.Point(198, 413);
            this.BtnDeleteNewspaper.Name = "BtnDeleteNewspaper";
            this.BtnDeleteNewspaper.Size = new System.Drawing.Size(110, 23);
            this.BtnDeleteNewspaper.TabIndex = 42;
            this.BtnDeleteNewspaper.Text = "Delete Newspaper";
            this.BtnDeleteNewspaper.UseVisualStyleBackColor = true;
            // 
            // dataGridNewspapers
            // 
            this.dataGridNewspapers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridNewspapers.Location = new System.Drawing.Point(14, 13);
            this.dataGridNewspapers.Name = "dataGridNewspapers";
            this.dataGridNewspapers.Size = new System.Drawing.Size(490, 208);
            this.dataGridNewspapers.TabIndex = 40;
            // 
            // lbArticles
            // 
            this.lbArticles.AutoSize = true;
            this.lbArticles.Location = new System.Drawing.Point(510, 10);
            this.lbArticles.Name = "lbArticles";
            this.lbArticles.Size = new System.Drawing.Size(55, 13);
            this.lbArticles.TabIndex = 53;
            this.lbArticles.Text = "All Articles";
            // 
            // BtnAddNewspaper
            // 
            this.BtnAddNewspaper.Location = new System.Drawing.Point(48, 391);
            this.BtnAddNewspaper.Name = "BtnAddNewspaper";
            this.BtnAddNewspaper.Size = new System.Drawing.Size(133, 40);
            this.BtnAddNewspaper.TabIndex = 41;
            this.BtnAddNewspaper.Text = "Add Newspaper";
            this.BtnAddNewspaper.UseVisualStyleBackColor = true;
            // 
            // LibraryNewspaperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 443);
            this.Controls.Add(this.lbArticles);
            this.Controls.Add(this.btnDeleteArticle);
            this.Controls.Add(this.lbArticlesCurrentNewspaper);
            this.Controls.Add(this.dataGridArticlesCurrentNewspaper);
            this.Controls.Add(this.dataGridAllArticles);
            this.Controls.Add(this.BtnUpdateNewspaper);
            this.Controls.Add(this.lbNameNewspaper);
            this.Controls.Add(this.tbNameNewspaper);
            this.Controls.Add(this.BtnFindNewspaper);
            this.Controls.Add(this.BtnDeleteNewspaper);
            this.Controls.Add(this.BtnAddNewspaper);
            this.Controls.Add(this.dataGridNewspapers);
            this.Name = "LibraryNewspaperForm";
            this.Text = "LibraryNewspaperForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentNewspaper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNewspapers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteArticle;
        private System.Windows.Forms.Label lbArticlesCurrentNewspaper;
        private System.Windows.Forms.DataGridView dataGridArticlesCurrentNewspaper;
        private System.Windows.Forms.DataGridView dataGridAllArticles;
        private System.Windows.Forms.Button BtnUpdateNewspaper;
        private System.Windows.Forms.Label lbNameNewspaper;
        private System.Windows.Forms.TextBox tbNameNewspaper;
        private System.Windows.Forms.Button BtnFindNewspaper;
        private System.Windows.Forms.Button BtnDeleteNewspaper;
        private System.Windows.Forms.DataGridView dataGridNewspapers;
        private System.Windows.Forms.Label lbArticles;
        private System.Windows.Forms.Button BtnAddNewspaper;
    }
}