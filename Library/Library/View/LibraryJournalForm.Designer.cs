﻿namespace Library.View
{
    partial class LibraryJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteArticle = new System.Windows.Forms.Button();
            this.btnNewJournal = new System.Windows.Forms.Button();
            this.lbArticlesCurrentJournal = new System.Windows.Forms.Label();
            this.dataGridArticlesCurrentJournal = new System.Windows.Forms.DataGridView();
            this.dataGridAllArticles = new System.Windows.Forms.DataGridView();
            this.BtnUpdateJournal = new System.Windows.Forms.Button();
            this.lbArticles = new System.Windows.Forms.Label();
            this.lbNameJournal = new System.Windows.Forms.Label();
            this.tbNameJournal = new System.Windows.Forms.TextBox();
            this.BtnFindJournal = new System.Windows.Forms.Button();
            this.BtnDeleteJournal = new System.Windows.Forms.Button();
            this.dataGridJournals = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJournals)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDeleteArticle
            // 
            this.btnDeleteArticle.Location = new System.Drawing.Point(278, 402);
            this.btnDeleteArticle.Name = "btnDeleteArticle";
            this.btnDeleteArticle.Size = new System.Drawing.Size(84, 23);
            this.btnDeleteArticle.TabIndex = 39;
            this.btnDeleteArticle.Text = "DeleteArticle";
            this.btnDeleteArticle.UseVisualStyleBackColor = true;
            // 
            // btnNewJournal
            // 
            this.btnNewJournal.Location = new System.Drawing.Point(14, 402);
            this.btnNewJournal.Name = "btnNewJournal";
            this.btnNewJournal.Size = new System.Drawing.Size(75, 23);
            this.btnNewJournal.TabIndex = 36;
            this.btnNewJournal.Text = "New Journal";
            this.btnNewJournal.UseVisualStyleBackColor = true;
            // 
            // lbArticlesCurrentJournal
            // 
            this.lbArticlesCurrentJournal.AutoSize = true;
            this.lbArticlesCurrentJournal.Location = new System.Drawing.Point(13, 224);
            this.lbArticlesCurrentJournal.Name = "lbArticlesCurrentJournal";
            this.lbArticlesCurrentJournal.Size = new System.Drawing.Size(123, 13);
            this.lbArticlesCurrentJournal.TabIndex = 35;
            this.lbArticlesCurrentJournal.Text = "Articles Selected Journal";
            // 
            // dataGridArticlesCurrentJournal
            // 
            this.dataGridArticlesCurrentJournal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridArticlesCurrentJournal.Location = new System.Drawing.Point(13, 240);
            this.dataGridArticlesCurrentJournal.Name = "dataGridArticlesCurrentJournal";
            this.dataGridArticlesCurrentJournal.Size = new System.Drawing.Size(555, 156);
            this.dataGridArticlesCurrentJournal.TabIndex = 34;
            // 
            // dataGridAllArticles
            // 
            this.dataGridAllArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllArticles.Location = new System.Drawing.Point(577, 26);
            this.dataGridAllArticles.Name = "dataGridAllArticles";
            this.dataGridAllArticles.Size = new System.Drawing.Size(368, 233);
            this.dataGridAllArticles.TabIndex = 33;
            // 
            // BtnUpdateJournal
            // 
            this.BtnUpdateJournal.Location = new System.Drawing.Point(95, 402);
            this.BtnUpdateJournal.Name = "BtnUpdateJournal";
            this.BtnUpdateJournal.Size = new System.Drawing.Size(86, 23);
            this.BtnUpdateJournal.TabIndex = 31;
            this.BtnUpdateJournal.Text = "UpdateJournal";
            this.BtnUpdateJournal.UseVisualStyleBackColor = true;
            // 
            // lbArticles
            // 
            this.lbArticles.AutoSize = true;
            this.lbArticles.Location = new System.Drawing.Point(574, 10);
            this.lbArticles.Name = "lbArticles";
            this.lbArticles.Size = new System.Drawing.Size(55, 13);
            this.lbArticles.TabIndex = 30;
            this.lbArticles.Text = "All Articles";
            // 
            // lbNameJournal
            // 
            this.lbNameJournal.AutoSize = true;
            this.lbNameJournal.Location = new System.Drawing.Point(605, 287);
            this.lbNameJournal.Name = "lbNameJournal";
            this.lbNameJournal.Size = new System.Drawing.Size(72, 13);
            this.lbNameJournal.TabIndex = 29;
            this.lbNameJournal.Text = "Name Journal";
            // 
            // tbNameJournal
            // 
            this.tbNameJournal.Location = new System.Drawing.Point(683, 280);
            this.tbNameJournal.Name = "tbNameJournal";
            this.tbNameJournal.Size = new System.Drawing.Size(171, 20);
            this.tbNameJournal.TabIndex = 28;
            // 
            // BtnFindJournal
            // 
            this.BtnFindJournal.Location = new System.Drawing.Point(605, 315);
            this.BtnFindJournal.Name = "BtnFindJournal";
            this.BtnFindJournal.Size = new System.Drawing.Size(241, 43);
            this.BtnFindJournal.TabIndex = 27;
            this.BtnFindJournal.Text = "Find Journal";
            this.BtnFindJournal.UseVisualStyleBackColor = true;
            // 
            // BtnDeleteJournal
            // 
            this.BtnDeleteJournal.Location = new System.Drawing.Point(191, 402);
            this.BtnDeleteJournal.Name = "BtnDeleteJournal";
            this.BtnDeleteJournal.Size = new System.Drawing.Size(81, 23);
            this.BtnDeleteJournal.TabIndex = 26;
            this.BtnDeleteJournal.Text = "DeleteJournal";
            this.BtnDeleteJournal.UseVisualStyleBackColor = true;
            // 
            // dataGridJournals
            // 
            this.dataGridJournals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJournals.Location = new System.Drawing.Point(13, 13);
            this.dataGridJournals.Name = "dataGridJournals";
            this.dataGridJournals.Size = new System.Drawing.Size(555, 208);
            this.dataGridJournals.TabIndex = 24;
            // 
            // LibraryJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 431);
            this.Controls.Add(this.btnDeleteArticle);
            this.Controls.Add(this.btnNewJournal);
            this.Controls.Add(this.lbArticlesCurrentJournal);
            this.Controls.Add(this.dataGridArticlesCurrentJournal);
            this.Controls.Add(this.dataGridAllArticles);
            this.Controls.Add(this.BtnUpdateJournal);
            this.Controls.Add(this.lbArticles);
            this.Controls.Add(this.lbNameJournal);
            this.Controls.Add(this.tbNameJournal);
            this.Controls.Add(this.BtnFindJournal);
            this.Controls.Add(this.BtnDeleteJournal);
            this.Controls.Add(this.dataGridJournals);
            this.Name = "LibraryJournalForm";
            this.Text = "LibraryJournalForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJournals)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteArticle;
        private System.Windows.Forms.Button btnNewJournal;
        private System.Windows.Forms.Label lbArticlesCurrentJournal;
        private System.Windows.Forms.DataGridView dataGridArticlesCurrentJournal;
        private System.Windows.Forms.DataGridView dataGridAllArticles;
        private System.Windows.Forms.Button BtnUpdateJournal;
        private System.Windows.Forms.Label lbArticles;
        private System.Windows.Forms.Label lbNameJournal;
        private System.Windows.Forms.TextBox tbNameJournal;
        private System.Windows.Forms.Button BtnFindJournal;
        private System.Windows.Forms.Button BtnDeleteJournal;
        private System.Windows.Forms.DataGridView dataGridJournals;
    }
}