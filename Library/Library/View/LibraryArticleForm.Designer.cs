﻿namespace Library.View
{
    partial class LibraryArticleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteAuthorArticle = new System.Windows.Forms.Button();
            this.btnNewArticle = new System.Windows.Forms.Button();
            this.lbAuthorsCurrentArticle = new System.Windows.Forms.Label();
            this.dataGridAuthorsCurrentArticle = new System.Windows.Forms.DataGridView();
            this.dataGridAllAuthors = new System.Windows.Forms.DataGridView();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.lbAuthor = new System.Windows.Forms.Label();
            this.lbTitleArticle = new System.Windows.Forms.Label();
            this.tbTitleArticle = new System.Windows.Forms.TextBox();
            this.BtnFindArticle = new System.Windows.Forms.Button();
            this.BtnDeleteArticle = new System.Windows.Forms.Button();
            this.dataGridArticles = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentArticle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticles)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDeleteAuthorArticle
            // 
            this.btnDeleteAuthorArticle.Location = new System.Drawing.Point(285, 403);
            this.btnDeleteAuthorArticle.Name = "btnDeleteAuthorArticle";
            this.btnDeleteAuthorArticle.Size = new System.Drawing.Size(84, 23);
            this.btnDeleteAuthorArticle.TabIndex = 39;
            this.btnDeleteAuthorArticle.Text = "DeleteAuthor";
            this.btnDeleteAuthorArticle.UseVisualStyleBackColor = true;
            // 
            // btnNewArticle
            // 
            this.btnNewArticle.Location = new System.Drawing.Point(13, 403);
            this.btnNewArticle.Name = "btnNewArticle";
            this.btnNewArticle.Size = new System.Drawing.Size(75, 23);
            this.btnNewArticle.TabIndex = 36;
            this.btnNewArticle.Text = "New Article";
            this.btnNewArticle.UseVisualStyleBackColor = true;
            this.btnNewArticle.Click += new System.EventHandler(this.btnNewArticle_Click);
            // 
            // lbAuthorsCurrentArticle
            // 
            this.lbAuthorsCurrentArticle.AutoSize = true;
            this.lbAuthorsCurrentArticle.Location = new System.Drawing.Point(24, 303);
            this.lbAuthorsCurrentArticle.Name = "lbAuthorsCurrentArticle";
            this.lbAuthorsCurrentArticle.Size = new System.Drawing.Size(120, 13);
            this.lbAuthorsCurrentArticle.TabIndex = 35;
            this.lbAuthorsCurrentArticle.Text = "Authors Selected Article";
            // 
            // dataGridAuthorsCurrentArticle
            // 
            this.dataGridAuthorsCurrentArticle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAuthorsCurrentArticle.Location = new System.Drawing.Point(12, 317);
            this.dataGridAuthorsCurrentArticle.Name = "dataGridAuthorsCurrentArticle";
            this.dataGridAuthorsCurrentArticle.Size = new System.Drawing.Size(555, 80);
            this.dataGridAuthorsCurrentArticle.TabIndex = 34;
            // 
            // dataGridAllAuthors
            // 
            this.dataGridAllAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllAuthors.Location = new System.Drawing.Point(576, 27);
            this.dataGridAllAuthors.Name = "dataGridAllAuthors";
            this.dataGridAllAuthors.Size = new System.Drawing.Size(368, 233);
            this.dataGridAllAuthors.TabIndex = 33;
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.Location = new System.Drawing.Point(94, 403);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(87, 23);
            this.BtnUpdate.TabIndex = 31;
            this.BtnUpdate.Text = "Update Article";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            // 
            // lbAuthor
            // 
            this.lbAuthor.AutoSize = true;
            this.lbAuthor.Location = new System.Drawing.Point(573, 11);
            this.lbAuthor.Name = "lbAuthor";
            this.lbAuthor.Size = new System.Drawing.Size(57, 13);
            this.lbAuthor.TabIndex = 30;
            this.lbAuthor.Text = "All Authors";
            // 
            // lbTitleArticle
            // 
            this.lbTitleArticle.AutoSize = true;
            this.lbTitleArticle.Location = new System.Drawing.Point(577, 320);
            this.lbTitleArticle.Name = "lbTitleArticle";
            this.lbTitleArticle.Size = new System.Drawing.Size(56, 13);
            this.lbTitleArticle.TabIndex = 29;
            this.lbTitleArticle.Text = "Title Aricle";
            // 
            // tbTitleArticle
            // 
            this.tbTitleArticle.Location = new System.Drawing.Point(646, 313);
            this.tbTitleArticle.Name = "tbTitleArticle";
            this.tbTitleArticle.Size = new System.Drawing.Size(171, 20);
            this.tbTitleArticle.TabIndex = 28;
            // 
            // BtnFindArticle
            // 
            this.BtnFindArticle.Location = new System.Drawing.Point(576, 354);
            this.BtnFindArticle.Name = "BtnFindArticle";
            this.BtnFindArticle.Size = new System.Drawing.Size(241, 43);
            this.BtnFindArticle.TabIndex = 27;
            this.BtnFindArticle.Text = "Find Article";
            this.BtnFindArticle.UseVisualStyleBackColor = true;
            // 
            // BtnDeleteArticle
            // 
            this.BtnDeleteArticle.Location = new System.Drawing.Point(198, 403);
            this.BtnDeleteArticle.Name = "BtnDeleteArticle";
            this.BtnDeleteArticle.Size = new System.Drawing.Size(75, 23);
            this.BtnDeleteArticle.TabIndex = 26;
            this.BtnDeleteArticle.Text = "Delete Article";
            this.BtnDeleteArticle.UseVisualStyleBackColor = true;
            // 
            // dataGridArticles
            // 
            this.dataGridArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridArticles.Location = new System.Drawing.Point(12, 14);
            this.dataGridArticles.Name = "dataGridArticles";
            this.dataGridArticles.Size = new System.Drawing.Size(555, 282);
            this.dataGridArticles.TabIndex = 24;
            // 
            // LibraryArticleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 435);
            this.Controls.Add(this.btnDeleteAuthorArticle);
            this.Controls.Add(this.btnNewArticle);
            this.Controls.Add(this.lbAuthorsCurrentArticle);
            this.Controls.Add(this.dataGridAuthorsCurrentArticle);
            this.Controls.Add(this.dataGridAllAuthors);
            this.Controls.Add(this.BtnUpdate);
            this.Controls.Add(this.lbAuthor);
            this.Controls.Add(this.lbTitleArticle);
            this.Controls.Add(this.tbTitleArticle);
            this.Controls.Add(this.BtnFindArticle);
            this.Controls.Add(this.BtnDeleteArticle);
            this.Controls.Add(this.dataGridArticles);
            this.Name = "LibraryArticleForm";
            this.Text = "LibraryArticleFolrm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentArticle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteAuthorArticle;
        private System.Windows.Forms.Button btnNewArticle;
        private System.Windows.Forms.Label lbAuthorsCurrentArticle;
        private System.Windows.Forms.DataGridView dataGridAuthorsCurrentArticle;
        private System.Windows.Forms.DataGridView dataGridAllAuthors;
        private System.Windows.Forms.Button BtnUpdate;
        private System.Windows.Forms.Label lbAuthor;
        private System.Windows.Forms.Label lbTitleArticle;
        private System.Windows.Forms.TextBox tbTitleArticle;
        private System.Windows.Forms.Button BtnFindArticle;
        private System.Windows.Forms.Button BtnDeleteArticle;
        private System.Windows.Forms.DataGridView dataGridArticles;
    }
}