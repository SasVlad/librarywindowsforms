﻿namespace Library.View
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenBookLibrary = new System.Windows.Forms.Button();
            this.btnOpenArticleLibrary = new System.Windows.Forms.Button();
            this.btnOpenJournalLibrary = new System.Windows.Forms.Button();
            this.btnOpenNewspaperLibrary = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenBookLibrary
            // 
            this.btnOpenBookLibrary.Location = new System.Drawing.Point(12, 40);
            this.btnOpenBookLibrary.Name = "btnOpenBookLibrary";
            this.btnOpenBookLibrary.Size = new System.Drawing.Size(222, 62);
            this.btnOpenBookLibrary.TabIndex = 0;
            this.btnOpenBookLibrary.Text = "Open Book Library";
            this.btnOpenBookLibrary.UseVisualStyleBackColor = true;
            this.btnOpenBookLibrary.Click += new System.EventHandler(this.btnOpenBookLibrary_Click);
            // 
            // btnOpenArticleLibrary
            // 
            this.btnOpenArticleLibrary.Location = new System.Drawing.Point(12, 116);
            this.btnOpenArticleLibrary.Name = "btnOpenArticleLibrary";
            this.btnOpenArticleLibrary.Size = new System.Drawing.Size(222, 62);
            this.btnOpenArticleLibrary.TabIndex = 1;
            this.btnOpenArticleLibrary.Text = "Open Article Library";
            this.btnOpenArticleLibrary.UseVisualStyleBackColor = true;
            this.btnOpenArticleLibrary.Click += new System.EventHandler(this.btnOpenArticleLibrary_Click);
            // 
            // btnOpenJournalLibrary
            // 
            this.btnOpenJournalLibrary.Location = new System.Drawing.Point(12, 184);
            this.btnOpenJournalLibrary.Name = "btnOpenJournalLibrary";
            this.btnOpenJournalLibrary.Size = new System.Drawing.Size(222, 59);
            this.btnOpenJournalLibrary.TabIndex = 2;
            this.btnOpenJournalLibrary.Text = "Open Journal Library";
            this.btnOpenJournalLibrary.UseVisualStyleBackColor = true;
            this.btnOpenJournalLibrary.Click += new System.EventHandler(this.btnOpenJournalLibrary_Click);
            // 
            // btnOpenNewspaperLibrary
            // 
            this.btnOpenNewspaperLibrary.Location = new System.Drawing.Point(12, 251);
            this.btnOpenNewspaperLibrary.Name = "btnOpenNewspaperLibrary";
            this.btnOpenNewspaperLibrary.Size = new System.Drawing.Size(222, 60);
            this.btnOpenNewspaperLibrary.TabIndex = 3;
            this.btnOpenNewspaperLibrary.Text = "Open Newspaper Library";
            this.btnOpenNewspaperLibrary.UseVisualStyleBackColor = true;
            this.btnOpenNewspaperLibrary.Click += new System.EventHandler(this.btnOpenNewspaperLibrary_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 337);
            this.Controls.Add(this.btnOpenNewspaperLibrary);
            this.Controls.Add(this.btnOpenJournalLibrary);
            this.Controls.Add(this.btnOpenArticleLibrary);
            this.Controls.Add(this.btnOpenBookLibrary);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenBookLibrary;
        private System.Windows.Forms.Button btnOpenArticleLibrary;
        private System.Windows.Forms.Button btnOpenJournalLibrary;
        private System.Windows.Forms.Button btnOpenNewspaperLibrary;
    }
}