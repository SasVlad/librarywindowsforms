﻿using Library.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Model;
using Library.Presentation;
using Library.View;

namespace Library
{
    public partial class LibraryBookForm : Form, ILibraryBookView
    {
        public event Action FindBook;
        public event Action RemoveBook;
        public event Action UpdateBook;
        public event Action DeleteAuthorBook;

        private List<BookViewModel> _allBooks;
        private List<AuthorModel> _allAuthors;
        private List<AuthorModel> _allBookAuthors = new List<AuthorModel>();
        private bool _flagChageTable = true;
        List<string> listAuthors = new List<string>();
        public LibraryBookForm()
        {
            InitializeComponent();
            
            BtnFindBook.Click+= (sender, args) => Invoke(FindBook);
            BtnDeleteBook.Click += (sender, args) => Invoke(RemoveBook);

            BtnUpdate.Click+= (sender, args) => Invoke(UpdateBook);
            this.Load+=(sender, args) => Invoke(FindBook);
            
            dataGridBooks.SelectionChanged+= ChangeIndexInDatagridBook_Event;
            btnDeleteAuthorArticle.Click += (sender, args) => Invoke(DeleteAuthorBook);
        }

        public BookViewModel GetBook
        {
            get {
                listAuthors.Clear();
                var _listAuthors = dataGridAllAuthors.CurrentCell == null ? new List<AuthorModel>() : new List<AuthorModel>() {
                    (AuthorModel)dataGridAllAuthors.CurrentRow.DataBoundItem };
                return new BookViewModel
                {
                    Book = new BookModel { Name = tbNameBook.Text },
                    Authors = _listAuthors
                
                };
            }
            
        }
        
        public AuthorModel GetCurrentAuthorBook
        {
            get
            {
                return dataGridAuthorsCurrentBook.CurrentCell == null ? new AuthorModel() : (AuthorModel)dataGridAuthorsCurrentBook.CurrentRow.DataBoundItem;

            }

        }
        public BookViewModel GetBookToDelete
        {
            get
            {
                listAuthors.Clear();
                var _book = dataGridBooks.CurrentCell == null ? new BookModel() : (BookModel)dataGridBooks.CurrentRow.DataBoundItem;
                return new BookViewModel
                {
                    Book = _book,
                    Authors = _allBookAuthors
                };
            }

        }
        public int BookDataGridIndex {
            get
            {
                return this.dataGridBooks.CurrentRow.Index;
            }
        }

        public List<AuthorModel> GetAllAuthor
        {
            get
            {
                return _allAuthors;
            }
            set
            {
                if (_flagChageTable)
                {
                    _allAuthors= value;
                    dataGridAllAuthors.DataSource = value;
                    this.dataGridAllAuthors.CurrentCell = null;
                    dataGridAllAuthors.Columns["Id"].Visible = false;

                    _flagChageTable = false;
                }
            }
        }
        private void FillDataSource(List<BookModel> listBooks, List<AuthorModel> listAuthorModel)
        {
            dataGridAuthorsCurrentBook.DataSource = listAuthorModel;
            dataGridBooks.DataSource = listBooks;
            dataGridAuthorsCurrentBook.Columns["Id"].Visible = false;
            dataGridBooks.Columns["Id"].Visible = false;
            this.dataGridAllAuthors.CurrentCell = null;
        }
        public List<BookViewModel> LibraryBooks
        {
            get {
                return _allBooks;
            }
            set
            {
                
                dataGridBooks.ClearSelection();
                _allBooks = value;
                var _listBooks = new List<BookModel>();

                value.ForEach(x => {
                    _listBooks.Add(x.Book);
                });
                if (value.Count != 0)
                {
                    FillDataSource(_listBooks, value[0].Authors);
                }
                if (value.Count == 0)
                {
                    FillDataSource(new List<BookModel>(), new List<AuthorModel>());
                }

      
                
            }
        }
        public BookModel CurrentBook
        {
            get
            {
                if (dataGridBooks.Rows.Count!=0)
                {
                    return (BookModel)dataGridBooks.CurrentRow.DataBoundItem;
                }
                
                    MessageBox.Show("Data Grid is empty");
                    return null;
                
                
            }
        }

        private void Invoke(Action action)
        {
            if (action != null) action();
        }

        //--------------------chaged event
        private void ChangeIndexInDatagridBook_Event(object sender, EventArgs e)
        {
            if (this.dataGridBooks.CurrentRow !=null && _allBooks!=null)
            {
                dataGridAuthorsCurrentBook.DataSource = _allBooks[BookDataGridIndex].Authors;
            }
        }

        private void btnNewBook_Click(object sender, EventArgs e)
        {
           
            new AddNewBookPresentation(new AddNewBook()).Run();
            Invoke(FindBook);
        }

        private void btnArticleLibrary_Click(object sender, EventArgs e)
        {
            new LibraryArticlePresenter(new LibraryArticleForm()).Run();
            this.Close();
        }
    }

}

