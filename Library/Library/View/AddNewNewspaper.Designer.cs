﻿namespace Library.View
{
    partial class AddNewNewspaper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelect = new System.Windows.Forms.Button();
            this.BtnAddNewspaper = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridAllArticles = new System.Windows.Forms.DataGridView();
            this.dataGridArticlesCurrentNewspaper = new System.Windows.Forms.DataGridView();
            this.tbMainTheme = new System.Windows.Forms.TextBox();
            this.tbTypeNewspaper = new System.Windows.Forms.TextBox();
            this.tbPublisher = new System.Windows.Forms.TextBox();
            this.tbEdition = new System.Windows.Forms.TextBox();
            this.tbCountPages = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentNewspaper)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(305, 208);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(92, 33);
            this.btnSelect.TabIndex = 46;
            this.btnSelect.Text = "Select Article";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelectArticle_Click);
            // 
            // BtnAddNewspaper
            // 
            this.BtnAddNewspaper.Location = new System.Drawing.Point(21, 467);
            this.BtnAddNewspaper.Name = "BtnAddNewspaper";
            this.BtnAddNewspaper.Size = new System.Drawing.Size(139, 38);
            this.BtnAddNewspaper.TabIndex = 45;
            this.BtnAddNewspaper.Text = "AddNewspaper";
            this.BtnAddNewspaper.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(302, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "All articles";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 283);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(306, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Select articles from table \"All articles\" and press \"Select Article\"";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "MainTheme (str)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "TypeNewspaper (str)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Publisher (str)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Edition (int)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "CountPages (int)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Name (str)";
            // 
            // dataGridAllArticles
            // 
            this.dataGridAllArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllArticles.Location = new System.Drawing.Point(305, 39);
            this.dataGridAllArticles.Name = "dataGridAllArticles";
            this.dataGridAllArticles.Size = new System.Drawing.Size(271, 150);
            this.dataGridAllArticles.TabIndex = 36;
            // 
            // dataGridArticlesCurrentNewspaper
            // 
            this.dataGridArticlesCurrentNewspaper.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridArticlesCurrentNewspaper.Location = new System.Drawing.Point(21, 299);
            this.dataGridArticlesCurrentNewspaper.Name = "dataGridArticlesCurrentNewspaper";
            this.dataGridArticlesCurrentNewspaper.Size = new System.Drawing.Size(240, 150);
            this.dataGridArticlesCurrentNewspaper.TabIndex = 35;
            // 
            // tbMainTheme
            // 
            this.tbMainTheme.Location = new System.Drawing.Point(121, 221);
            this.tbMainTheme.Name = "tbMainTheme";
            this.tbMainTheme.Size = new System.Drawing.Size(100, 20);
            this.tbMainTheme.TabIndex = 34;
            // 
            // tbTypeNewspaper
            // 
            this.tbTypeNewspaper.Location = new System.Drawing.Point(121, 186);
            this.tbTypeNewspaper.Name = "tbTypeNewspaper";
            this.tbTypeNewspaper.Size = new System.Drawing.Size(100, 20);
            this.tbTypeNewspaper.TabIndex = 33;
            // 
            // tbPublisher
            // 
            this.tbPublisher.Location = new System.Drawing.Point(121, 151);
            this.tbPublisher.Name = "tbPublisher";
            this.tbPublisher.Size = new System.Drawing.Size(100, 20);
            this.tbPublisher.TabIndex = 32;
            // 
            // tbEdition
            // 
            this.tbEdition.Location = new System.Drawing.Point(121, 113);
            this.tbEdition.Name = "tbEdition";
            this.tbEdition.Size = new System.Drawing.Size(100, 20);
            this.tbEdition.TabIndex = 31;
            // 
            // tbCountPages
            // 
            this.tbCountPages.Location = new System.Drawing.Point(121, 74);
            this.tbCountPages.Name = "tbCountPages";
            this.tbCountPages.Size = new System.Drawing.Size(100, 20);
            this.tbCountPages.TabIndex = 30;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(121, 39);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 29;
            // 
            // AddNewNewspaper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 508);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.BtnAddNewspaper);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridAllArticles);
            this.Controls.Add(this.dataGridArticlesCurrentNewspaper);
            this.Controls.Add(this.tbMainTheme);
            this.Controls.Add(this.tbTypeNewspaper);
            this.Controls.Add(this.tbPublisher);
            this.Controls.Add(this.tbEdition);
            this.Controls.Add(this.tbCountPages);
            this.Controls.Add(this.tbName);
            this.Name = "AddNewNewspaper";
            this.Text = "AddNewNewspaper";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentNewspaper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button BtnAddNewspaper;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridAllArticles;
        private System.Windows.Forms.DataGridView dataGridArticlesCurrentNewspaper;
        private System.Windows.Forms.TextBox tbMainTheme;
        private System.Windows.Forms.TextBox tbTypeNewspaper;
        private System.Windows.Forms.TextBox tbPublisher;
        private System.Windows.Forms.TextBox tbEdition;
        private System.Windows.Forms.TextBox tbCountPages;
        private System.Windows.Forms.TextBox tbName;
    }
}