﻿using Library.Interfaces;
using Library.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library.View
{
    public partial class AddNewBook : Form, IAddNewBookPresentation
    {
        public event Action AddBook;
        public event Action FindBook;
        public event Action AddAuthor;

        private List<AuthorModel> _allAuthors;
        private List<AuthorModel> _allBookAuthors = new List<AuthorModel>();
        private bool _flagChageTable = true;
        public AddNewBook()
        {
           InitializeComponent();
            BtnAddBook.Click += (sender, args) =>
            {
                var _condition = @tbCountPages.Text != string.Empty && tbCover.Text != string.Empty 
                && tbEdition.Text != string.Empty &&tbName.Text != string.Empty&&tbPublisher.Text != string.Empty&&
                tbYeatOfBook.Text != string.Empty && _allBookAuthors.Count != 0;

                MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        _flagChageTable = true;
                        Invoke(AddBook);
                        this.Close();
                    },
                    "Input correct data or input not all fields");
            };
            this.Load += (sender, args) => Invoke(FindBook);

            btnAddNewAuthor.Click += (sender, args) => {

                var _condition = tbAuthorFirstName.Text != string.Empty && tbAuthorLastName.Text != string.Empty;
                MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        _flagChageTable = true;
                        Invoke(AddAuthor);
                    },
                    "Input correct data or input not all fields");
            };
        }

        public AuthorModel GetAuthorToAdd
        {
            get
            {
                return new AuthorModel {FirstName=tbAuthorFirstName.Text,LastName=tbAuthorLastName.Text };
            }

        }

        public BookViewModel GetBookToAdd
        {
            get
            {
                return new BookViewModel
                {
                    Book = new BookModel {Name= tbName.Text,CountPages=Convert.ToInt32(tbCountPages.Text),Edition= Convert.ToInt32(tbEdition.Text),
                    Publisher = tbPublisher.Text,Cover=tbCover.Text,YearOfBook= Convert.ToInt32(tbYeatOfBook.Text)},
                    Authors = _allBookAuthors
                };
            }

        }

        public List<AuthorModel> GetAllAuthor
        {
            get
            {
                return _allAuthors;
            }
            set
            {
                if (_flagChageTable&& value.Count != 0)
                {
                        _allAuthors = value;
                        dataGridAllAuthors.DataSource = value;
                        this.dataGridAllAuthors.CurrentCell = null;
                        dataGridAllAuthors.Columns["Id"].Visible = false;
                        _flagChageTable = false;                   
                }
            }
        }
        

        private void Invoke(Action action)
        {
            if (action != null) action();
        }
        public new void Show()
        {
            this.ShowDialog();
        }

        private void btnSelectAuthor_Click(object sender, EventArgs e)
        {
            if (_allBookAuthors == null)
            {
                _allBookAuthors = new List<AuthorModel>();
            }
            var _condition = dataGridAllAuthors.CurrentRow != null;
            MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        dataGridAuthorsCurrentBook.ClearSelection();
                        _allBookAuthors.Add((AuthorModel)dataGridAllAuthors.CurrentRow.DataBoundItem);

                        dataGridAuthorsCurrentBook.DataSource = null;
                        dataGridAuthorsCurrentBook.DataSource = _allBookAuthors;
                        dataGridAuthorsCurrentBook.Columns["Id"].Visible = false;
                    },
                    "Choose author");
           
            
        }
    }
}
