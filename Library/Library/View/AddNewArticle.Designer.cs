﻿namespace Library.View
{
    partial class AddNewArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbAuthorLastName = new System.Windows.Forms.TextBox();
            this.tbAuthorFirstName = new System.Windows.Forms.TextBox();
            this.btnAddNewAuthor = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.BtnAddArticle = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbBody = new System.Windows.Forms.Label();
            this.lbImage = new System.Windows.Forms.Label();
            this.lb = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridAllAuthors = new System.Windows.Forms.DataGridView();
            this.dataGridAuthorsCurrentArticle = new System.Windows.Forms.DataGridView();
            this.tbBody = new System.Windows.Forms.TextBox();
            this.tbImage = new System.Windows.Forms.TextBox();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentArticle)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(414, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Last Name (str)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(414, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "First Name (str)";
            // 
            // tbAuthorLastName
            // 
            this.tbAuthorLastName.Location = new System.Drawing.Point(492, 235);
            this.tbAuthorLastName.Name = "tbAuthorLastName";
            this.tbAuthorLastName.Size = new System.Drawing.Size(100, 20);
            this.tbAuthorLastName.TabIndex = 49;
            // 
            // tbAuthorFirstName
            // 
            this.tbAuthorFirstName.Location = new System.Drawing.Point(492, 200);
            this.tbAuthorFirstName.Name = "tbAuthorFirstName";
            this.tbAuthorFirstName.Size = new System.Drawing.Size(100, 20);
            this.tbAuthorFirstName.TabIndex = 48;
            // 
            // btnAddNewAuthor
            // 
            this.btnAddNewAuthor.Location = new System.Drawing.Point(472, 278);
            this.btnAddNewAuthor.Name = "btnAddNewAuthor";
            this.btnAddNewAuthor.Size = new System.Drawing.Size(120, 23);
            this.btnAddNewAuthor.TabIndex = 47;
            this.btnAddNewAuthor.Text = "Add New Author";
            this.btnAddNewAuthor.UseVisualStyleBackColor = true;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(316, 200);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(92, 33);
            this.btnSelect.TabIndex = 46;
            this.btnSelect.Text = "Select Author";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelectAuthor_Click);
            // 
            // BtnAddArticle
            // 
            this.BtnAddArticle.Location = new System.Drawing.Point(33, 391);
            this.BtnAddArticle.Name = "BtnAddArticle";
            this.BtnAddArticle.Size = new System.Drawing.Size(139, 38);
            this.BtnAddArticle.TabIndex = 45;
            this.BtnAddArticle.Text = "Add Article";
            this.BtnAddArticle.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(318, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "All authors";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(312, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Select authors from table \"All authors\" and press \"Select Author\"";
            // 
            // lbBody
            // 
            this.lbBody.AutoSize = true;
            this.lbBody.Location = new System.Drawing.Point(50, 146);
            this.lbBody.Name = "lbBody";
            this.lbBody.Size = new System.Drawing.Size(51, 13);
            this.lbBody.TabIndex = 40;
            this.lbBody.Text = "Body (str)";
            // 
            // lbImage
            // 
            this.lbImage.AutoSize = true;
            this.lbImage.Location = new System.Drawing.Point(50, 115);
            this.lbImage.Name = "lbImage";
            this.lbImage.Size = new System.Drawing.Size(56, 13);
            this.lbImage.TabIndex = 39;
            this.lbImage.Text = "Image (str)";
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.Location = new System.Drawing.Point(50, 72);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(47, 13);
            this.lb.TabIndex = 38;
            this.lb.Text = "Title (str)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Name (str)";
            // 
            // dataGridAllAuthors
            // 
            this.dataGridAllAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllAuthors.Location = new System.Drawing.Point(321, 34);
            this.dataGridAllAuthors.Name = "dataGridAllAuthors";
            this.dataGridAllAuthors.Size = new System.Drawing.Size(271, 150);
            this.dataGridAllAuthors.TabIndex = 36;
            // 
            // dataGridAuthorsCurrentArticle
            // 
            this.dataGridAuthorsCurrentArticle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAuthorsCurrentArticle.Location = new System.Drawing.Point(33, 223);
            this.dataGridAuthorsCurrentArticle.Name = "dataGridAuthorsCurrentArticle";
            this.dataGridAuthorsCurrentArticle.Size = new System.Drawing.Size(240, 150);
            this.dataGridAuthorsCurrentArticle.TabIndex = 35;
            // 
            // tbBody
            // 
            this.tbBody.Location = new System.Drawing.Point(128, 146);
            this.tbBody.Name = "tbBody";
            this.tbBody.Size = new System.Drawing.Size(100, 20);
            this.tbBody.TabIndex = 32;
            // 
            // tbImage
            // 
            this.tbImage.Location = new System.Drawing.Point(128, 108);
            this.tbImage.Name = "tbImage";
            this.tbImage.Size = new System.Drawing.Size(100, 20);
            this.tbImage.TabIndex = 31;
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(128, 69);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(100, 20);
            this.tbTitle.TabIndex = 30;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 34);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 29;
            // 
            // AddNewArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 445);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbAuthorLastName);
            this.Controls.Add(this.tbAuthorFirstName);
            this.Controls.Add(this.btnAddNewAuthor);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.BtnAddArticle);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbBody);
            this.Controls.Add(this.lbImage);
            this.Controls.Add(this.lb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridAllAuthors);
            this.Controls.Add(this.dataGridAuthorsCurrentArticle);
            this.Controls.Add(this.tbBody);
            this.Controls.Add(this.tbImage);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.textBox1);
            this.Name = "AddNewArticle";
            this.Text = "AddNewArticle";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentArticle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbAuthorLastName;
        private System.Windows.Forms.TextBox tbAuthorFirstName;
        private System.Windows.Forms.Button btnAddNewAuthor;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button BtnAddArticle;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbBody;
        private System.Windows.Forms.Label lbImage;
        private System.Windows.Forms.Label lb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridAllAuthors;
        private System.Windows.Forms.DataGridView dataGridAuthorsCurrentArticle;
        private System.Windows.Forms.TextBox tbBody;
        private System.Windows.Forms.TextBox tbImage;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.TextBox textBox1;
    }
}