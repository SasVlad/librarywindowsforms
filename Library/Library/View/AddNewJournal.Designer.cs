﻿namespace Library.View
{
    partial class AddNewJournal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAddJournal = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbType = new System.Windows.Forms.TextBox();
            this.tbCover = new System.Windows.Forms.TextBox();
            this.tbPublisher = new System.Windows.Forms.TextBox();
            this.tbEdition = new System.Windows.Forms.TextBox();
            this.tbCountPages = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridAllArticles = new System.Windows.Forms.DataGridView();
            this.dataGridArticlesCurrentJournal = new System.Windows.Forms.DataGridView();
            this.lbGenre = new System.Windows.Forms.Label();
            this.tbGenre = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentJournal)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnAddJournal
            // 
            this.BtnAddJournal.Location = new System.Drawing.Point(35, 457);
            this.BtnAddJournal.Name = "BtnAddJournal";
            this.BtnAddJournal.Size = new System.Drawing.Size(139, 38);
            this.BtnAddJournal.TabIndex = 45;
            this.BtnAddJournal.Text = "Add Journal";
            this.BtnAddJournal.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Type (str)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Cover (str)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Publisher (str)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Edition (int)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "CountPages (int)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Name (str)";
            // 
            // tbType
            // 
            this.tbType.Location = new System.Drawing.Point(123, 211);
            this.tbType.Name = "tbType";
            this.tbType.Size = new System.Drawing.Size(100, 20);
            this.tbType.TabIndex = 34;
            // 
            // tbCover
            // 
            this.tbCover.Location = new System.Drawing.Point(123, 176);
            this.tbCover.Name = "tbCover";
            this.tbCover.Size = new System.Drawing.Size(100, 20);
            this.tbCover.TabIndex = 33;
            // 
            // tbPublisher
            // 
            this.tbPublisher.Location = new System.Drawing.Point(123, 141);
            this.tbPublisher.Name = "tbPublisher";
            this.tbPublisher.Size = new System.Drawing.Size(100, 20);
            this.tbPublisher.TabIndex = 32;
            // 
            // tbEdition
            // 
            this.tbEdition.Location = new System.Drawing.Point(123, 103);
            this.tbEdition.Name = "tbEdition";
            this.tbEdition.Size = new System.Drawing.Size(100, 20);
            this.tbEdition.TabIndex = 31;
            // 
            // tbCountPages
            // 
            this.tbCountPages.Location = new System.Drawing.Point(123, 64);
            this.tbCountPages.Name = "tbCountPages";
            this.tbCountPages.Size = new System.Drawing.Size(100, 20);
            this.tbCountPages.TabIndex = 30;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(123, 29);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 29;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(305, 194);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(92, 33);
            this.btnSelect.TabIndex = 51;
            this.btnSelect.Text = "Select Article";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelectArticle_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(302, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "All articles";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 269);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(306, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "Select articles from table \"All articles\" and press \"Select Article\"";
            // 
            // dataGridAllArticles
            // 
            this.dataGridAllArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllArticles.Location = new System.Drawing.Point(305, 25);
            this.dataGridAllArticles.Name = "dataGridAllArticles";
            this.dataGridAllArticles.Size = new System.Drawing.Size(271, 150);
            this.dataGridAllArticles.TabIndex = 48;
            // 
            // dataGridArticlesCurrentJournal
            // 
            this.dataGridArticlesCurrentJournal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridArticlesCurrentJournal.Location = new System.Drawing.Point(21, 285);
            this.dataGridArticlesCurrentJournal.Name = "dataGridArticlesCurrentJournal";
            this.dataGridArticlesCurrentJournal.Size = new System.Drawing.Size(240, 150);
            this.dataGridArticlesCurrentJournal.TabIndex = 47;
            // 
            // lbGenre
            // 
            this.lbGenre.AutoSize = true;
            this.lbGenre.Location = new System.Drawing.Point(32, 244);
            this.lbGenre.Name = "lbGenre";
            this.lbGenre.Size = new System.Drawing.Size(56, 13);
            this.lbGenre.TabIndex = 53;
            this.lbGenre.Text = "Genre (str)";
            // 
            // tbGenre
            // 
            this.tbGenre.Location = new System.Drawing.Point(123, 237);
            this.tbGenre.Name = "tbGenre";
            this.tbGenre.Size = new System.Drawing.Size(100, 20);
            this.tbGenre.TabIndex = 52;
            // 
            // AddNewJournal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 505);
            this.Controls.Add(this.lbGenre);
            this.Controls.Add(this.tbGenre);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dataGridAllArticles);
            this.Controls.Add(this.dataGridArticlesCurrentJournal);
            this.Controls.Add(this.BtnAddJournal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbType);
            this.Controls.Add(this.tbCover);
            this.Controls.Add(this.tbPublisher);
            this.Controls.Add(this.tbEdition);
            this.Controls.Add(this.tbCountPages);
            this.Controls.Add(this.tbName);
            this.Name = "AddNewJournal";
            this.Text = "AddNewJournal";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllArticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArticlesCurrentJournal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtnAddJournal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbType;
        private System.Windows.Forms.TextBox tbCover;
        private System.Windows.Forms.TextBox tbPublisher;
        private System.Windows.Forms.TextBox tbEdition;
        private System.Windows.Forms.TextBox tbCountPages;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridAllArticles;
        private System.Windows.Forms.DataGridView dataGridArticlesCurrentJournal;
        private System.Windows.Forms.Label lbGenre;
        private System.Windows.Forms.TextBox tbGenre;
    }
}