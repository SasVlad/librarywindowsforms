﻿namespace Library.View
{
    partial class AddNewBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbCountPages = new System.Windows.Forms.TextBox();
            this.tbEdition = new System.Windows.Forms.TextBox();
            this.tbPublisher = new System.Windows.Forms.TextBox();
            this.tbCover = new System.Windows.Forms.TextBox();
            this.tbYeatOfBook = new System.Windows.Forms.TextBox();
            this.dataGridAuthorsCurrentBook = new System.Windows.Forms.DataGridView();
            this.dataGridAllAuthors = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnAddBook = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnAddNewAuthor = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbAuthorLastName = new System.Windows.Forms.TextBox();
            this.tbAuthorFirstName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(131, 23);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 0;
            // 
            // tbCountPages
            // 
            this.tbCountPages.Location = new System.Drawing.Point(131, 58);
            this.tbCountPages.Name = "tbCountPages";
            this.tbCountPages.Size = new System.Drawing.Size(100, 20);
            this.tbCountPages.TabIndex = 1;
            // 
            // tbEdition
            // 
            this.tbEdition.Location = new System.Drawing.Point(131, 97);
            this.tbEdition.Name = "tbEdition";
            this.tbEdition.Size = new System.Drawing.Size(100, 20);
            this.tbEdition.TabIndex = 2;
            // 
            // tbPublisher
            // 
            this.tbPublisher.Location = new System.Drawing.Point(131, 135);
            this.tbPublisher.Name = "tbPublisher";
            this.tbPublisher.Size = new System.Drawing.Size(100, 20);
            this.tbPublisher.TabIndex = 3;
            // 
            // tbCover
            // 
            this.tbCover.Location = new System.Drawing.Point(131, 170);
            this.tbCover.Name = "tbCover";
            this.tbCover.Size = new System.Drawing.Size(100, 20);
            this.tbCover.TabIndex = 4;
            // 
            // tbYeatOfBook
            // 
            this.tbYeatOfBook.Location = new System.Drawing.Point(131, 205);
            this.tbYeatOfBook.Name = "tbYeatOfBook";
            this.tbYeatOfBook.Size = new System.Drawing.Size(100, 20);
            this.tbYeatOfBook.TabIndex = 5;
            // 
            // dataGridAuthorsCurrentBook
            // 
            this.dataGridAuthorsCurrentBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAuthorsCurrentBook.Location = new System.Drawing.Point(32, 283);
            this.dataGridAuthorsCurrentBook.Name = "dataGridAuthorsCurrentBook";
            this.dataGridAuthorsCurrentBook.Size = new System.Drawing.Size(240, 150);
            this.dataGridAuthorsCurrentBook.TabIndex = 6;
            // 
            // dataGridAllAuthors
            // 
            this.dataGridAllAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAllAuthors.Location = new System.Drawing.Point(316, 23);
            this.dataGridAllAuthors.Name = "dataGridAllAuthors";
            this.dataGridAllAuthors.Size = new System.Drawing.Size(271, 150);
            this.dataGridAllAuthors.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Name (str)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "CountPages (int)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Edition (int)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Publisher (str)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Cover (str)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "YearOfBook (int)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(312, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Select authors from table \"All authors\" and press \"Select Author\"";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(313, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "All authors";
            // 
            // BtnAddBook
            // 
            this.BtnAddBook.Location = new System.Drawing.Point(32, 451);
            this.BtnAddBook.Name = "BtnAddBook";
            this.BtnAddBook.Size = new System.Drawing.Size(139, 38);
            this.BtnAddBook.TabIndex = 16;
            this.BtnAddBook.Text = "AddBook";
            this.BtnAddBook.UseVisualStyleBackColor = true;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(277, 192);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(92, 33);
            this.btnSelect.TabIndex = 22;
            this.btnSelect.Text = "Select Author";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelectAuthor_Click);
            // 
            // btnAddNewAuthor
            // 
            this.btnAddNewAuthor.Location = new System.Drawing.Point(467, 267);
            this.btnAddNewAuthor.Name = "btnAddNewAuthor";
            this.btnAddNewAuthor.Size = new System.Drawing.Size(120, 23);
            this.btnAddNewAuthor.TabIndex = 24;
            this.btnAddNewAuthor.Text = "Add New Author";
            this.btnAddNewAuthor.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(409, 227);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Last Name (str)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(409, 192);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "First Name (str)";
            // 
            // tbAuthorLastName
            // 
            this.tbAuthorLastName.Location = new System.Drawing.Point(487, 224);
            this.tbAuthorLastName.Name = "tbAuthorLastName";
            this.tbAuthorLastName.Size = new System.Drawing.Size(100, 20);
            this.tbAuthorLastName.TabIndex = 26;
            // 
            // tbAuthorFirstName
            // 
            this.tbAuthorFirstName.Location = new System.Drawing.Point(487, 189);
            this.tbAuthorFirstName.Name = "tbAuthorFirstName";
            this.tbAuthorFirstName.Size = new System.Drawing.Size(100, 20);
            this.tbAuthorFirstName.TabIndex = 25;
            // 
            // AddNewBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 499);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbAuthorLastName);
            this.Controls.Add(this.tbAuthorFirstName);
            this.Controls.Add(this.btnAddNewAuthor);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.BtnAddBook);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridAllAuthors);
            this.Controls.Add(this.dataGridAuthorsCurrentBook);
            this.Controls.Add(this.tbYeatOfBook);
            this.Controls.Add(this.tbCover);
            this.Controls.Add(this.tbPublisher);
            this.Controls.Add(this.tbEdition);
            this.Controls.Add(this.tbCountPages);
            this.Controls.Add(this.tbName);
            this.Name = "AddNewBook";
            this.Text = "AddNewBook";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAuthorsCurrentBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAllAuthors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbCountPages;
        private System.Windows.Forms.TextBox tbEdition;
        private System.Windows.Forms.TextBox tbPublisher;
        private System.Windows.Forms.TextBox tbCover;
        private System.Windows.Forms.TextBox tbYeatOfBook;
        private System.Windows.Forms.DataGridView dataGridAuthorsCurrentBook;
        private System.Windows.Forms.DataGridView dataGridAllAuthors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnAddBook;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnAddNewAuthor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbAuthorLastName;
        private System.Windows.Forms.TextBox tbAuthorFirstName;
    }
}