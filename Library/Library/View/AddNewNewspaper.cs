﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library.View
{
    public partial class AddNewNewspaper : Form, IAddNewNewspaperPresentation
    {
        public event Action AddNewspaper;
        public event Action GetFreeNewspaperArticles;

        private List<ArticleModel> _allArticles;
        private List<ArticleModel> _allNewspaperArticles = new List<ArticleModel>();
        private bool _flagChageTable = true;
        List<string> listArticles = new List<string>();
        public AddNewNewspaper()
        {
            InitializeComponent();
            

            BtnAddNewspaper.Click += (sender, args) =>
            {
                var _condition = @tbCountPages.Text != string.Empty && tbTypeNewspaper.Text != string.Empty
                   && tbEdition.Text != string.Empty && tbName.Text != string.Empty && tbPublisher.Text != string.Empty &&
                   tbMainTheme.Text != string.Empty  && _allNewspaperArticles.Count != 0;

                MyCustomCondition.ChooseCondition(
                                _condition,
                                () =>
                                {
                                    _flagChageTable = true;
                                    Invoke(AddNewspaper);
                                    this.Close();
                                },
                        "Input correct data or input not all fields");
                

            };

            this.Load += (sender, args) => Invoke(GetFreeNewspaperArticles);
        }


        public ArticleModel GetArticleToAdd
        {
            get
            {
                return dataGridAllArticles.CurrentCell == null ? new ArticleModel() : (ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem;

            }

        }

        public NewspaperViewModel GetNewspaperToAdd
        {
            get
            {
                listArticles.Clear();
                
                return new NewspaperViewModel
                {
                    Newspaper = new NewspaperModel {CountPages=Convert.ToInt32(tbCountPages.Text),Edition= Convert.ToInt32(tbEdition.Text),MainTheme=tbMainTheme.Text,Name=tbName.Text,Publisher=tbPublisher.Text,TypeNewspaper=tbTypeNewspaper.Text },
                    Articles = _allNewspaperArticles
                };
            }

        }


        public List<ArticleModel> GetAllArticle
        {
            get
            {
                return _allArticles;
            }
            set
            {
                if (_flagChageTable)
                {
                    _allArticles = value;
                    dataGridAllArticles.DataSource = value;
                    this.dataGridAllArticles.CurrentCell = null;
                    dataGridAllArticles.Columns["Id"].Visible = false;

                    _flagChageTable = false;
                }
            }
        }



        private void Invoke(Action action)
        {
            if (action != null) action();
        }
        public new void Show()
        {
            this.ShowDialog();
        }
        //--------------------chaged event


        private void btnSelectArticle_Click(object sender, EventArgs e)
        {
            if (_allNewspaperArticles == null)
            {
                _allNewspaperArticles = new List<ArticleModel>();
            }
            var _condition = dataGridArticlesCurrentNewspaper.CurrentRow != null;
            MyCustomCondition.ChooseCondition(
                    _condition,
                    () =>
                    {
                        dataGridArticlesCurrentNewspaper.ClearSelection();
                        _allNewspaperArticles.Add((ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem);

                        dataGridArticlesCurrentNewspaper.DataSource = null;
                        dataGridArticlesCurrentNewspaper.DataSource = _allNewspaperArticles;
                        if (dataGridArticlesCurrentNewspaper.RowCount != 0)
                        {
                            dataGridArticlesCurrentNewspaper.Columns["Id"].Visible = false;
                        }
                    },
                    "Choose article");
            
        }
    }
}
