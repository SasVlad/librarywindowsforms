﻿using Library.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Model;
using Library.Model.ViewModel;
using Library.Presentation;

namespace Library.View
{
    public partial class LibraryJournalForm : Form, ILibraryJournalView
    {
        public event Action FindJournal;
        public event Action RemoveJournal;
        public event Action UpdateJournal;
        public event Action DeleteArticleJournal;

        private List<JournalViewModel> _allJournals;
        private List<ArticleModel> _allArticles;
        private List<ArticleModel> _allJournalArticles = new List<ArticleModel>();
        private bool _flagChageTable = true;
        List<string> listArticles = new List<string>();
        public LibraryJournalForm()
        {
            InitializeComponent();

            BtnFindJournal.Click += (sender, args) => Invoke(FindJournal);
            BtnDeleteJournal.Click += (sender, args) => Invoke(RemoveJournal);

            BtnUpdateJournal.Click += (sender, args) => Invoke(UpdateJournal);
            this.Load += (sender, args) => Invoke(FindJournal);
            btnNewJournal.Click += (sender, args) => {
                new AddNewJournalPresentation(new AddNewJournal()).Run();
                Invoke(FindJournal);
            };
            dataGridJournals.SelectionChanged += ChangeIndexInDatagridJournal_Event;
            btnDeleteArticle.Click += (sender, args) => Invoke(DeleteArticleJournal);
        }

        public JournalViewModel GetJournal
        {
            get
            {
                listArticles.Clear();
                var _listArticles = dataGridAllArticles.CurrentCell == null ? new List<ArticleModel>() : new List<ArticleModel>() {
                    (ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem };
                return new JournalViewModel
                {
                    Journal = new JournalModel { Name = tbNameJournal.Text },
                    Articles = _listArticles

                };
            }

        }
        public ArticleModel GetArticleToAdd
        {
            get
            {
                return dataGridAllArticles.CurrentCell == null ? new ArticleModel() : (ArticleModel)dataGridAllArticles.CurrentRow.DataBoundItem;

            }

        }
        public ArticleModel GetCurrentArticleJournal
        {
            get
            {
                return dataGridArticlesCurrentJournal.CurrentCell == null ? new ArticleModel() : (ArticleModel)dataGridArticlesCurrentJournal.CurrentRow.DataBoundItem;

            }

        }

        public int JournalDataGridIndex
        {
            get
            {
                return this.dataGridJournals.CurrentRow.Index;
            }
        }

        public List<ArticleModel> GetAllArticle
        {
            get
            {
                return _allArticles;
            }
            set
            {
                if (_flagChageTable)
                {
                    _allArticles = value;
                    dataGridAllArticles.DataSource = value;
                    this.dataGridAllArticles.CurrentCell = null;
                    if (dataGridAllArticles.ColumnCount != 0)
                    {
                        dataGridAllArticles.Columns["Id"].Visible = false;
                    }

                    _flagChageTable = false;
                }
            }
        }
        private void FillDataSource(List<JournalModel> listJournal, List<ArticleModel> listArtistModel)
        {
            dataGridArticlesCurrentJournal.DataSource = listArtistModel;
            dataGridJournals.DataSource = listJournal;
            dataGridArticlesCurrentJournal.Columns["Id"].Visible = false;
            dataGridJournals.Columns["Id"].Visible = false;
            this.dataGridAllArticles.CurrentCell = null;
        }

        public List<JournalViewModel> LibraryJournals
        {
            get
            {
                return _allJournals;
            }
            set
            {
                
                    dataGridJournals.ClearSelection();
                    _allJournals = value;
                    var _listJournals = new List<JournalModel>();

                    value.ForEach(x =>
                    {
                        _listJournals.Add(x.Journal);
                    });
                    

                    if (value.Count != 0)
                    {
                    FillDataSource(_listJournals, value[0].Articles);
                    }

                    if (value.Count == 0)
                    {
                    FillDataSource(new List<JournalModel>(), new List<ArticleModel>());
                    }
                        
              }
                
            
        }
        public JournalModel CurrentJournal
        {
            get
            {
                if (dataGridJournals.Rows.Count != 0)
                {
                    return (JournalModel)dataGridJournals.CurrentRow.DataBoundItem;
                }

                MessageBox.Show("Data Grid is empty");
                return null;


            }
        }
        public JournalViewModel GetJournalToDelete
        {
            get
            {
                listArticles.Clear();
                var _book = dataGridJournals.CurrentCell == null ? new JournalModel() : (JournalModel)dataGridJournals.CurrentRow.DataBoundItem;
                return new JournalViewModel
                {
                    Journal = _book,
                    Articles = _allJournalArticles
                };
            }

        }
        private void Invoke(Action action)
        {
            if (action != null) action();
        }

        //--------------------chaged event
        private void ChangeIndexInDatagridJournal_Event(object sender, EventArgs e)
        {
            if (this.dataGridJournals.CurrentRow != null && _allJournals != null)
            {
                dataGridArticlesCurrentJournal.DataSource = _allJournals[JournalDataGridIndex].Articles;
            }
        }

    }
}
