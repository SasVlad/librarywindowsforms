﻿using Library.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library.View
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOpenBookLibrary_Click(object sender, EventArgs e)
        {
            var presenter = new LibraryBookPresenter(new LibraryBookForm()); // Dependency Injection
        }

        private void btnOpenArticleLibrary_Click(object sender, EventArgs e)
        {
            var presenter = new LibraryArticlePresenter(new LibraryArticleForm()); // Dependency Injection
        }

        private void btnOpenJournalLibrary_Click(object sender, EventArgs e)
        {
            var presenter = new LibraryJournalPresenter(new LibraryJournalForm()); // Dependency Injection
        }

        private void btnOpenNewspaperLibrary_Click(object sender, EventArgs e)
        {
            var presenter = new LibraryNewspaperPresenter(new LibraryNewspaperForm()); // Dependency Injection
        }
    }
}
