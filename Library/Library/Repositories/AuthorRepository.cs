﻿using Dapper;
using Library.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class AuthorRepository
    {
        string _connectionString;
        public AuthorRepository()
        {
                _connectionString = InitialDatabase.GetConnectionString();

        }
        public List<AuthorModel> GetAuthors()
        {
            var _authors = new List<AuthorModel>();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _authors = db.Query<AuthorModel>("SELECT * FROM Authors").ToList();
            }

            return _authors;
        }
        public List<AuthorModel> GetAllDistinctAuthors()
        {
            var _bookAuthors = new List<AuthorModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _bookAuthors = db.Query<AuthorModel>(@"SELECT DISTINCT a.Id,a.FirstName,a.LastName FROM Authors a").ToList();
            }
            return _bookAuthors;
        }
        public List<AuthorModel> GetAuthorsByBookId(int bookId)
        {
            var _bookAuthors = new List<AuthorModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _bookAuthors = db.Query<AuthorModel>(@"SELECT a.Id, a.FirstName,a.LastName FROM Authors a
                                                    inner join
                                                        AuthorPolygraphies ap
                                                    on
                                                        a.Id=ap.AuthorId_FK where ap.BookId_FK = @bookId", new { bookId }).ToList();
            }
            return _bookAuthors;
        }
        public List<AuthorModel> GetAuthorsByArticleId(int articleId)
        {
            var _articleAuthors = new List<AuthorModel>();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _articleAuthors = db.Query<AuthorModel>(@"SELECT a.Id, a.FirstName,a.LastName FROM Authors a
                                                    inner join
                                                        AuthorPolygraphies ap
                                                    on
                                                        a.Id=ap.AuthorId_FK where ap.ArticleId_FK = @articleId", new { articleId }).ToList();
            }
            return _articleAuthors;
        }

        public AuthorModel GetAuthorById(int id)
        {
            var _author = new AuthorModel();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _author = db.Query<AuthorModel>("SELECT * FROM Authors WHERE Id = @id", new { id }).FirstOrDefault();
            }

            return _author;
        }       
       

        public int CreateAuthor(AuthorModel author)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "INSERT INTO Authors(FirstName, LastName) VALUES(@FirstName, @LastName); SELECT SCOPE_IDENTITY()";
                return db.Query<int>(sqlQuery, author).SingleOrDefault();
            }
        }

        public void UpdateAuthor(AuthorModel author)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlQuery = "UPDATE Authors SET FirstName = @FirstName, LastName = @LastName Id = @Id";
                db.Execute(_sqlQuery, author);
            }
        }

        public void DeleteAuthorById(int authorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlauthor = "DELETE FROM Authors WHERE Id = @authorId";
                db.Execute(_sqlauthor, new { authorId });
            }
        }
    }
}
