﻿using Dapper;
using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class NewspaperRepository
    {
        bool init = false;
        string _connectionString;
        ArticleRepository _articleRepository;
        public NewspaperRepository()
        {
            _articleRepository = new ArticleRepository();
            _connectionString = InitialDatabase.GetConnectionString();


            
            if (!init)
            {
                
                
                init = true;

            }
        }
        public void SeedNewspaperTable()
        {

            var newspaper1 = new NewspaperModel { Name = "Asp.Net Mvc", MainTheme = "sss", Publisher = "Apress", TypeNewspaper = "MvcNewspaper", CountPages = 1547,  Edition = 1541 };
            var newspaper2 = new NewspaperModel { Name = "Asp.Net WebApi2", MainTheme = "aaa", Publisher = "Apress", TypeNewspaper = "WebApi2Newspaper", CountPages = 2547,Edition = 2541 };
            var newspaper3 = new NewspaperModel { Name = "C# via .net", MainTheme = "bbb", Publisher = "Apress", TypeNewspaper = "netNewspaper", CountPages = 3547,  Edition = 3541 };

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _newspaper1Id = this.CreateNewspaper(new NewspaperViewModel { Newspaper = newspaper1, Articles = new List<ArticleModel> { new ArticleModel { Id = 1 }, new ArticleModel { Id = 2 } } });
                var _newspaper2Id = this.CreateNewspaper(new NewspaperViewModel { Newspaper = newspaper2, Articles = new List<ArticleModel> { new ArticleModel { Id = 3 } } });
                var _newspaper3Id = this.CreateNewspaper(new NewspaperViewModel { Newspaper = newspaper3, Articles = new List<ArticleModel> { } });
            }
        }

        public List<NewspaperViewModel> GetNewspapers()
        {
            var _newspapers = new List<NewspaperModel>();
            var _newspapersViewModel = new List<NewspaperViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _newspapers = db.Query<NewspaperModel>("SELECT * FROM Newspapers").ToList();
            }
            foreach (var newspaper in _newspapers)
            {
                _newspapersViewModel.Add(new NewspaperViewModel
                {
                    Newspaper = newspaper,
                    Articles = _articleRepository.GetArticlesByNewspaperId(newspaper.Id)
                });
            }
            return _newspapersViewModel;
        }

        public NewspaperViewModel GetNewspaperById(int id)
        {
            var _newspaper = new NewspaperModel();
            var _newspaperArticles = new List<ArticleModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _newspaper = db.Query<NewspaperModel>("SELECT * FROM Newspapers WHERE Id = @id", new { id }).FirstOrDefault();

                _newspaperArticles = _articleRepository.GetArticlesByNewspaperId(id);
            }

            return new NewspaperViewModel { Newspaper = _newspaper, Articles = _newspaperArticles };
        }

        public NewspaperViewModel GetNewspaperByNewspaperName(string newspaperName)
        {
            var _newspaper = new NewspaperModel();
            var _newspaperArticles = new List<ArticleModel>();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _newspaper = db.Query<NewspaperModel>("SELECT * FROM Newspapers WHERE Name = @newspaperName", new { newspaperName }).FirstOrDefault();

                _newspaperArticles = _articleRepository.GetArticlesByNewspaperId(_newspaper.Id);
            }

            return new NewspaperViewModel { Newspaper = _newspaper, Articles = _newspaperArticles };
        }


        public List<NewspaperViewModel> GetNewspaperByNewspaperArticles(List<ArticleModel> newspaperArticles)
        {
            var _newspapers = new List<NewspaperModel>();
            var _newspaperArticles = new List<NewspaperViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {

                foreach (var article in newspaperArticles)
                {
                    _newspapers = db.Query<NewspaperModel>(@"SELECT * FROM Newspapers
                                                       where Id = @NewspaperId_FK", new { article.NewspaperId_FK }).ToList();

                    foreach (var newspaper in _newspapers)
                    {
                        _newspaperArticles.Add(new NewspaperViewModel { Newspaper = newspaper, Articles = new List<ArticleModel> { article } });
                    }
                }
            }

            return _newspaperArticles;
        }
        public int CreateNewspaper(NewspaperViewModel newspaper)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "INSERT INTO Newspapers(Name,CountPages,Edition,MainTheme,Publisher,TypeNewspaper) VALUES(@Name,@CountPages,@Edition,@MainTheme,@Publisher,@TypeNewspaper);SELECT SCOPE_IDENTITY()";
                var newspaperId = db.Query<int>(sqlQuery, newspaper.Newspaper).FirstOrDefault();
                foreach (var article in newspaper.Articles)
                {
                    AddArticleToNewspaperById(article.Id, newspaperId);
                }
                return newspaperId;
            }
        }

        public void UpdateNewspaper(NewspaperModel newspaper)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlQuery = "UPDATE Newspapers SET Name = @Name,CountPages= @CountPages,Edition=@Edition,MainTheme = @MainTheme, Publisher = @Publisher, TypeNewspaper = @TypeNewspaper where Id = @Id";
                db.Execute(_sqlQuery, newspaper);
            }
        }
        public void AddArticleToNewspaperById(int articleId, int newspaperId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlarticle = "UPDATE Articles SET NewspaperId_FK = @newspaperId where Id = @articleId";
                db.Execute(_sqlarticle, new { newspaperId, articleId });

            }
        }
        public void DeleteArticleNewspaperById(int articleId, int newspaperId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlarticle = "UPDATE Articles SET NewspaperId_FK = NULL where Id = @articleId AND NewspaperId_FK = @newspaperId";
                db.Execute(_sqlarticle, new { newspaperId, articleId });

            }
        }


        public void DeleteNewspaperById(int newspaperId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _articleIds = db.Query<int>("Select Id FROM Articles WHERE NewspaperId_FK = @newspaperId", new { newspaperId });
                foreach (var _articleId in _articleIds)
                {
                    DeleteArticleNewspaperById(_articleId, newspaperId);
                }

                var _sqlQuery = "DELETE FROM Newspapers WHERE Id = @newspaperId";
                db.Execute(_sqlQuery, new { newspaperId });
            }
        }
    }
}
