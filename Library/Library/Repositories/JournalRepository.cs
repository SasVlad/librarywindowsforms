﻿using Dapper;
using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class JournalRepository
    {
        string _connectionString;
        ArticleRepository _articleRepository;
        public JournalRepository()
        {
            _articleRepository = new ArticleRepository();
            _connectionString = InitialDatabase.GetConnectionString();
         
        }

        public List<JournalViewModel> GetJournals()
        {
            var _journals = new List<JournalModel>();
            var _journalsViewModel = new List<JournalViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _journals = db.Query<JournalModel>("SELECT * FROM Journals").ToList();
            }
            foreach (var journal in _journals)
            {
                _journalsViewModel.Add(new JournalViewModel
                {
                    Journal = journal,
                    Articles = _articleRepository.GetArticlesByJournalId(journal.Id)
                });
            }
            return _journalsViewModel;
        }

        public JournalViewModel GetJournalById(int id)
        {
            var _journal = new JournalModel();
            var _journalArticles = new List<ArticleModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _journal = db.Query<JournalModel>("SELECT * FROM Journals WHERE Id = @id", new { id }).FirstOrDefault();

                _journalArticles = _articleRepository.GetArticlesByJournalId(id);
            }

            return new JournalViewModel { Journal = _journal, Articles = _journalArticles };
        }

        public JournalViewModel GetJournalByJournalName(string journalName)
        {
            var _journal = new JournalModel();
            var _journalArticles = new List<ArticleModel>();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _journal = db.Query<JournalModel>("SELECT * FROM Journals WHERE Name = @journalName", new { journalName }).FirstOrDefault();

                _journalArticles = _articleRepository.GetArticlesByJournalId(_journal.Id);
            }

            return new JournalViewModel { Journal = _journal, Articles = _journalArticles };
        }


        public List<JournalViewModel> GetJournalByJournalArticles(List<ArticleModel> journalArticles)
        {
            var _journals = new List<JournalModel>();
            var _journalArticles = new List<JournalViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {

                foreach (var article in journalArticles)
                {
                    _journals = db.Query<JournalModel>(@"SELECT * FROM Journals
                                                       where Id = @JournalId_FK", new { article.JournalId_FK }).ToList();

                    foreach (var journal in _journals)
                    {
                        _journalArticles.Add(new JournalViewModel { Journal = journal, Articles = new List<ArticleModel> { article } });
                    }
                }
            }

            return _journalArticles;
        }
        public int CreateJournal(JournalViewModel journal)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "INSERT INTO Journals(Name,CountPages,Edition,Genre,Publisher,Type,Cover) VALUES(@Name,@CountPages,@Edition,@Genre,@Publisher,@Type,@Cover);SELECT SCOPE_IDENTITY()";
                var journalId = db.Query<int>(sqlQuery, journal.Journal).FirstOrDefault();
                foreach (var article in journal.Articles)
                {
                    AddArticleToJournalById(article.Id, journalId);
                }
                return journalId;                
            }
        }

        public void UpdateJournal(JournalModel journal)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlQuery = "UPDATE Journals SET Name = @Name,CountPages= @CountPages,Edition=@Edition,Genre = @Genre, Publisher = @Publisher, Type = @Type,Cover=@Cover where Id = @Id";
                db.Execute(_sqlQuery, journal);
            }
        }
        public void AddArticleToJournalById(int articleId, int journalId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlarticle = "UPDATE Articles SET JournalId_FK = @journalId where Id = @articleId";
                db.Execute(_sqlarticle, new { journalId, articleId });

            }
        }
        public void DeleteArticleJournalById(int articleId, int journalId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlarticle = "UPDATE Articles SET JournalId_FK = NULL where Id = @articleId AND JournalId_FK = @journalId";
                db.Execute(_sqlarticle, new { journalId, articleId });

            }
        }


        public void DeleteJournalById(int journalId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _articleIds = db.Query<int>("Select Id FROM Articles WHERE JournalId_FK = @journalId", new { journalId });               
                foreach (var _articleId in _articleIds)
                {
                    DeleteArticleJournalById(_articleId, journalId);
                }

                var _sqlQuery = "DELETE FROM Journals WHERE Id = @journalId";
                db.Execute(_sqlQuery, new { journalId });
            }
        }
    }
}
