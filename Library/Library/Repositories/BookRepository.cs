﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.IO;
using Library.Repositories;
using System.Runtime.InteropServices;

namespace Library.Model
{
    public class BookRepositoryDb
    {
        string _connectionString ;
        AuthorRepository _authorRepository;
        AuthorPolygraphyRepository _authorPolygraphyRepository;
        public BookRepositoryDb()
        {
            _authorRepository = new AuthorRepository();
            _authorPolygraphyRepository = new AuthorPolygraphyRepository();

            _connectionString = InitialDatabase.GetConnectionString();                               

        }
        

        public List<BookViewModel> GetBooks()
        {
            var _books = new List<BookModel>();
            var _booksViewModel = new List<BookViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _books = db.Query<BookModel>("SELECT * FROM Books").ToList();
            }
            foreach (var book in _books)
            {
                _booksViewModel.Add(new BookViewModel {
                    Book =book,
                    Authors = _authorRepository.GetAuthorsByBookId(book.Id)
                });
            }
            return _booksViewModel;
        }        

        public BookViewModel GetBookById(int id)
        {
            var _book = new BookModel();
            var _bookAuthors = new List<AuthorModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _book = db.Query<BookModel>("SELECT * FROM Books WHERE Id = @id", new { id }).FirstOrDefault();

                _bookAuthors = _authorRepository.GetAuthorsByBookId(id);
            }
            
            return new BookViewModel { Book=_book,Authors=_bookAuthors};
        }

        public BookViewModel GetBookByBookName(string bookName)
        {
            var _book = new BookModel();
            var _bookAuthors = new List<AuthorModel>();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _book = db.Query<BookModel>("SELECT * FROM Books WHERE Name = @bookName", new { bookName }).FirstOrDefault();

                _bookAuthors = _authorRepository.GetAuthorsByBookId(_book.Id);
            }

            return new BookViewModel { Book = _book, Authors = _bookAuthors };
        }


    public List<BookViewModel> GetBookByBookAuthors(List<AuthorModel> bookAuthors)
    {
        var _books = new List<BookModel>();
        var _bookAuthors = new List<BookViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
        {

            foreach (var author in bookAuthors)
            {
                _books = db.Query<BookModel>(@"SELECT b.Id, b.Name,b.YearOfBook,b.Publisher,b.Edition,b.CountPages,b.Cover FROM Books b
                                                    inner join
                                                        AuthorPolygraphies ap
                                                    on
                                                        b.Id=ap.BookId_FK where ap.AuthorId_FK = @Id", new { author.Id }).ToList();
                    foreach (var _book in _books)
                    {
                        _bookAuthors.Add(new BookViewModel { Book = _book, Authors=_authorRepository.GetAuthorsByBookId(_book.Id)});
                    }
            }
        }

        return _bookAuthors;
    }
    public void CreateBook(BookViewModel book)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "INSERT INTO Books(Name,YearOfBook,Publisher,Edition,CountPages,Cover) VALUES(@Name,@YearOfBook,@Publisher,@Edition,@CountPages,@Cover);SELECT SCOPE_IDENTITY()";
                var bookId = db.Query<int>(sqlQuery, book.Book).SingleOrDefault();
                book.Book.Id = bookId;
                
                foreach (var author in book.Authors)
                {   
                    _authorPolygraphyRepository.CreateAuthorPolygraphy( new AuthorPolygraphyModel { BookId_FK = bookId, AuthorId_Fk = author.Id });
                }                               
            }
        }

        public void UpdateBook(BookModel book)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlQuery = "UPDATE Books SET Name = @Name, YearOfBook = @YearOfBook, Publisher = @Publisher, Edition = @Edition, CountPages = @CountPages, Cover = @Cover where Id = @Id";
                db.Execute(_sqlQuery, book);
            }
        }

        public void DeleteAuthorBookById(int authorId, int bookId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlauthor = "DELETE FROM AuthorPolygraphies WHERE BookId_FK = @bookId AND AuthorId_FK = @authorId";               
                db.Execute(_sqlauthor, new { bookId, authorId });

                DeleteAutorIfBookNotExist(authorId);
            }
        }

        public void DeleteAutorIfBookNotExist(int authorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                if (db.Query<int>("Select Count(Id) From AuthorPolygraphies WHERE AuthorId_FK = @authorId", new { authorId }).FirstOrDefault() == 0)
                    db.Execute("DELETE FROM Authors WHERE Id = @authorId", new { authorId });
            }
        }

        public void DeleteBookById(int bookId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _authorIds = db.Query<int>("Select AuthorId_FK FROM AuthorPolygraphies WHERE BookId_FK = @bookId", new { bookId });
                var _sqlauthor = "DELETE FROM AuthorPolygraphies WHERE BookId_FK = @bookId";
                var _sqlQuery = "DELETE FROM Books WHERE Id = @bookId";
                db.Execute(_sqlauthor, new { bookId });               
                db.Execute(_sqlQuery, new { bookId });
                foreach (var _authorId in _authorIds)
                {
                    DeleteAutorIfBookNotExist(_authorId);
                }
               
            }
        }

    }
}
