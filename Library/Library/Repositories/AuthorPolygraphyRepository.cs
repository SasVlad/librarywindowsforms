﻿using Dapper;
using Library.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class AuthorPolygraphyRepository
    {
        string _connectionString;
        //AuthorRepository _authorRepository;
        //ArticleRepositoryDb _bookRepository;
        public AuthorPolygraphyRepository()
        {
            //_authorRepository = new AuthorRepository();
            //_bookRepository = new ArticleRepositoryDb();          
                _connectionString = InitialDatabase.GetConnectionString();
            
        }
        public int GetCountAuthorsByAuthorId(int authorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return db.Query<int>("Select Count(Id) From AuthorPolygraphies WHERE AuthorId_FK = @authorId", new { authorId }).FirstOrDefault();
            }
        }

        public void CreateAuthorPolygraphy(AuthorPolygraphyModel authorPoligraphy)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                if (authorPoligraphy.BookId_FK != 0)
                {
                    db.Query<int>("INSERT INTO AuthorPolygraphies(BookId_FK, AuthorId_FK) VALUES(@BookId_FK, @AuthorId_Fk); SELECT SCOPE_IDENTITY()", authorPoligraphy);
                }
                if (authorPoligraphy.ArticleId_FK != 0)
                {
                    db.Query<int>("INSERT INTO AuthorPolygraphies(ArticleId_FK, AuthorId_FK) VALUES(@ArticleId_FK, @AuthorId_Fk); SELECT SCOPE_IDENTITY()", authorPoligraphy);
                }
            }
        }

        public void UpdateArticle(ArticleModel book)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlQuery = "UPDATE Articles SET Name = @Name, YearOfBook = @YearOfBook Publisher = @Publisher, Edition = @Edition CountPages = @CountPages, Cover = @Cover Id = @Id";
                db.Execute(_sqlQuery, book);
            }
        }

        public void DeleteAuthorArticleById(int authorId, int bookId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlauthor = "DELETE FROM AuthorPolygraphies WHERE BookId_FK = @bookId AND AuthorId_FK = @authorId";
                db.Execute(_sqlauthor, new { bookId, authorId });

                DeleteAutorIfArticleNotExist(authorId);
            }
        }

        public void DeleteAutorIfArticleNotExist(int authorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                if (db.Query<int>("Select Count(Id) From AuthorPolygraphies WHERE AuthorId_FK = @authorId", new { authorId }).FirstOrDefault() == 0)
                    db.Execute("DELETE FROM Authors WHERE Id = @authorId", new { authorId });
            }
        }

        public void DeleteArticleById(int bookId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _authorIds = db.Query<int>("Select AuthorId_FK FROM AuthorPolygraphies WHERE ArticleId_FK = @bookId", new { bookId });
                var _sqlauthor = "DELETE FROM AuthorPolygraphies WHERE BookId_FK = @bookId";
                var _sqlQuery = "DELETE FROM Books WHERE Id = @bookId";
                db.Execute(_sqlauthor, new { bookId });
                db.Execute(_sqlQuery, new { bookId });
                foreach (var _authorId in _authorIds)
                {
                    DeleteAutorIfArticleNotExist(_authorId);
                }

            }
        }
    }
}
