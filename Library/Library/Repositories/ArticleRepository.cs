﻿using Dapper;
using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class ArticleRepository
    {
        string _connectionString ;
        AuthorRepository _authorRepository;
        AuthorPolygraphyRepository _authorPolygraphyRepository;
        public ArticleRepository()
        {
            _authorRepository = new AuthorRepository();
            _authorPolygraphyRepository = new AuthorPolygraphyRepository();

            _connectionString = InitialDatabase.GetConnectionString();                                 
        }


        public List<ArticleViewModel> GetArticles()
        {
            var _articles = new List<ArticleModel>();
            var _articlesViewModel = new List<ArticleViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _articles = db.Query<ArticleModel>("SELECT * FROM Articles").ToList();
            }
            foreach (var article in _articles)
            {
                _articlesViewModel.Add(new ArticleViewModel {
                    Article =article,
                    Authors = _authorRepository.GetAuthorsByArticleId(article.Id)
                });
            }
            return _articlesViewModel;
        }
        public List<ArticleModel> GetFreeJournalArticles()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return db.Query<ArticleModel>("SELECT * FROM Articles where JournalId_FK IS NULL").ToList();
            }
        }
        public List<ArticleModel> GetFreeNewspaperArticles()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return db.Query<ArticleModel>("SELECT * FROM Articles where NewspaperId_FK IS NULL").ToList();
            }
        }
        public List<ArticleModel> GetAllArticles()
        {
            var _articles = new List<ArticleModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _articles = db.Query<ArticleModel>("SELECT * FROM Articles").ToList();
            }

            return _articles;
        }
        public ArticleViewModel GetArticleById(int id)
        {
            var _article = new ArticleModel();
            var _articleAuthors = new List<AuthorModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _article = db.Query<ArticleModel>("SELECT * FROM Articles WHERE Id = @id", new { id }).FirstOrDefault();

                _articleAuthors = _authorRepository.GetAuthorsByArticleId(id);
            }
            
            return new ArticleViewModel { Article=_article,Authors=_articleAuthors};
        }
        public List<ArticleModel> GetArticlesByJournalId(int journalId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return db.Query<ArticleModel>("SELECT * FROM Articles WHERE JournalId_FK = @journalId", new { journalId }).ToList();
            }
        }
        public List<ArticleModel> GetArticlesByNewspaperId(int newspaperId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                return db.Query<ArticleModel>("SELECT * FROM Articles WHERE NewspaperId_FK = @newspaperId", new { newspaperId }).ToList();
            }
        }
        public ArticleViewModel GetArticleByArticleTitle(string articleTitle)
        {
            var _article = new ArticleModel();
            var _articleAuthors = new List<AuthorModel>();

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                _article = db.Query<ArticleModel>("SELECT * FROM Articles WHERE Title = @articleTitle", new { articleTitle }).FirstOrDefault();

                _articleAuthors = _authorRepository.GetAuthorsByArticleId(_article.Id);
            }

            return new ArticleViewModel { Article = _article, Authors = _articleAuthors };
        }


    public List<ArticleViewModel> GetArticleByArticleAuthors(List<AuthorModel> articleAuthors)
    {
        var _articles = new List<ArticleModel>();
        var _articleAuthors = new List<ArticleViewModel>();
            using (IDbConnection db = new SqlConnection(_connectionString))
        {

            foreach (var author in articleAuthors)
            {
                _articles = db.Query<ArticleModel>(@"SELECT a.Id, a.Title,a.Image,a.Body FROM Articles a
                                                    inner join
                                                        AuthorPolygraphies ap
                                                    on
                                                        a.Id=ap.ArticleId_FK where ap.AuthorId_FK = @Id", new { author.Id }).ToList();
                    foreach (var _article in _articles)
                    {
                        _articleAuthors.Add(new ArticleViewModel { Article = _article, Authors=_authorRepository.GetAuthorsByArticleId(_article.Id)});
                    }
            }
        }

        return _articleAuthors;
    }
    public void CreateArticle(ArticleViewModel article)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var sqlQuery = "INSERT INTO Articles(Title,Image,Body) VALUES(@Title,@Image,@Body);SELECT SCOPE_IDENTITY()";
                var articleId = db.Query<int>(sqlQuery, article.Article).SingleOrDefault();
                article.Article.Id = articleId;
                
                foreach (var author in article.Authors)
                {   
                    _authorPolygraphyRepository.CreateAuthorPolygraphy( new AuthorPolygraphyModel { ArticleId_FK = articleId, AuthorId_Fk = author.Id });
                }                               
            }
        }

        public void UpdateArticle(ArticleModel article)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlQuery = "UPDATE Articles SET Title = @Title, Image = @Image, Body = @Body where Id = @Id";
                db.Execute(_sqlQuery, article);
            }
        }

        public void DeleteAuthorArticleById(int authorId, int articleId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _sqlauthor = "DELETE FROM AuthorPolygraphies WHERE ArticleId_FK = @articleId AND AuthorId_FK = @authorId";               
                db.Execute(_sqlauthor, new { articleId, authorId });

                DeleteAutorIfArticleNotExist(authorId);
            }
        }

        public void DeleteAutorIfArticleNotExist(int authorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                if (db.Query<int>("Select Count(Id) From AuthorPolygraphies WHERE AuthorId_FK = @authorId", new { authorId }).FirstOrDefault() == 0)
                    db.Execute("DELETE FROM Authors WHERE Id = @authorId", new { authorId });
            }
        }

        public void DeleteArticleById(int articleId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var _authorIds = db.Query<int>("Select AuthorId_FK FROM AuthorPolygraphies WHERE ArticleId_FK = @articleId", new { articleId });
                var _sqlauthor = "DELETE FROM AuthorPolygraphies WHERE ArticleId_FK = @articleId";
                var _sqlQuery = "DELETE FROM Articles WHERE Id = @articleId";
                db.Execute(_sqlauthor, new { articleId });               
                db.Execute(_sqlQuery, new { articleId });
                foreach (var _authorId in _authorIds)
                {
                    DeleteAutorIfArticleNotExist(_authorId);
                }
               
            }
        }
    }
}
