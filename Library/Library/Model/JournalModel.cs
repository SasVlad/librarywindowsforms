﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public class JournalModel: PolygraphyModel
    {
        public string Genre { get; set; }
        public string Type { get; set; }
        public string Cover { get; set; }
    }
}
