﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model.ViewModel
{
    public class ArticleViewModel
    {
        public ArticleViewModel()
        {
            Article = new ArticleModel();
            Authors = new List<AuthorModel>();
        }
        public ArticleModel Article { get; set; }
        public List<AuthorModel> Authors { get; set; }
    }
}
