﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public class BookViewModel
    {
        public BookViewModel()
        {
            Book = new BookModel();
            Authors = new List<AuthorModel>();
        }
        public BookModel Book { get; set; }
        public List<AuthorModel> Authors { get; set; }
    }
}
