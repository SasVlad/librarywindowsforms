﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model.ViewModel
{
    public class NewspaperViewModel
    {
        public NewspaperViewModel()
        {
            Newspaper = new NewspaperModel();
            Articles = new List<ArticleModel>();
        }
        public NewspaperModel Newspaper { get; set; }
        public List<ArticleModel> Articles { get; set; }
    }
}
