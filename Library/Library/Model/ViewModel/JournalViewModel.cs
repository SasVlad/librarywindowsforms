﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model.ViewModel
{
    public class JournalViewModel
    {
        public JournalViewModel()
        {
            Journal = new JournalModel();
            Articles = new List<ArticleModel>();
        }
        public JournalModel Journal { get; set; }
        public List<ArticleModel> Articles { get; set; }
    }
}
