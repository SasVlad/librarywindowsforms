﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public class NewspaperModel: PolygraphyModel
    {
        public string TypeNewspaper { get; set; }
        public string MainTheme { get; set; }
    }
}
