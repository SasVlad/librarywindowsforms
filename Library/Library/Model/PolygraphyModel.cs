﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public class PolygraphyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountPages { get; set; }
        public int Edition { get; set; }
        public string Publisher { get; set; }
    }
}
