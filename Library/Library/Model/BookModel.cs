﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Model
{
    public class BookModel:PolygraphyModel
    {
        public string Cover { get; set; }
        public int YearOfBook { get; set; }
    }
}
