﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public enum TypeOfPolygraphy
    {
        Book,
        Journal,
        Article,
        Newspaper
    }
}
