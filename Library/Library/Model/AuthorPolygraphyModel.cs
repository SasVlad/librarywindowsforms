﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    public class AuthorPolygraphyModel
    {
        public int Id { get; set; }
        public int BookId_FK { get; set; }
        public int? ArticleId_FK { get; set; }
        public int? AuthorId_Fk { get; set; }
    }
}
