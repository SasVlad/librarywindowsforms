﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class AddNewNewspaperPresentation
    {
        private IAddNewNewspaperPresentation _view;
        private NewspaperService _newspaperService;
        private ArticleService _articleService;
        public AddNewNewspaperPresentation(IAddNewNewspaperPresentation view)
        {
            _view = view;
            _newspaperService = new NewspaperService();
            _articleService = new ArticleService();
           // _view.FindNewspaper += () => FindNewspapers(_view.GetNewspaper);
            _view.AddNewspaper += () => AddNewspaper(_view.GetNewspaperToAdd);
            _view.GetFreeNewspaperArticles += () => GetFreeNewspaperArticles();

        }


        public void Run()
        {
            _view.Show();

        }
        public void Close()
        {
            _view.Close();

        }

        private void GetFreeNewspaperArticles()
        {
            _view.GetAllArticle = _articleService.GetAllFreeNewspaperArticles();
        }
        private void AddNewspaper(NewspaperViewModel newspaper)
        {
            _newspaperService.AddNewspaper(newspaper);
            _view.GetAllArticle = _articleService.GetAllArticles;
        }

    }
}
