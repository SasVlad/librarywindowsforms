﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class LibraryArticlePresenter
    {
        private ILibraryArticleView _view;
        private ArticleService _articleService;
        private AuthorService _authorService;
        public LibraryArticlePresenter(ILibraryArticleView view)
        {
            _view = view;
            _articleService = new ArticleService();
            _authorService = new AuthorService();
            _view.FindArticle += () => FindArticles(_view.GetArticle);
            _view.RemoveArticle += () => RemoveCurrentArticle(_view.CurrentArticle);
            _view.UpdateArticle += () => UpdateArticle(_view.CurrentArticle);
            _view.DeleteAuthorArticle += () => DeleteAuthorArticle(_view.GetCurrentAuthorArticle.Id, _view.GetArticleToDelete.Article.Id);
            _view.Show();
        }

        private void RemoveCurrentArticle(ArticleModel currentArticle)
        {
            _articleService.RemoveArticle(currentArticle);
            _view.LibraryArticles = _articleService.GetArticles;
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }

        public void Run()
        {
            _view.Show();
        }
        private void UpdateArticle(ArticleModel article)
        {
            _articleService.UpdateArticle(article);
            _view.LibraryArticles = _articleService.GetArticles;
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }

        private void AddArticle(ArticleViewModel article)
        {
            _articleService.AddArticle(article);
            _view.LibraryArticles = _articleService.GetArticles;
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }
        private void AddAuthor(AuthorModel author)
        {
            _authorService.CreateAuthor(author);
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }
        private void DeleteAuthorArticle(int authorId, int articleId)
        {
            _articleService.DeleteAuthorArticle(authorId, articleId);
            _view.LibraryArticles = _articleService.GetArticles;
        }
        private void FindArticles(ArticleViewModel article)
        {
            _view.LibraryArticles = _articleService.FindArticles(article);
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }
    }
}
