﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class LibraryNewspaperPresenter
    {
        private ILibraryNewspaperView _view;
        private NewspaperService _newspaperService;
        private ArticleService _articleService;
        public LibraryNewspaperPresenter(ILibraryNewspaperView view)
        {
            _view = view;
            _newspaperService = new NewspaperService();
            _articleService = new ArticleService();
            _view.FindNewspaper += () => FindNewspapers(_view.GetNewspaper);
            _view.RemoveNewspaper += () => RemoveCurrentNewspaper(_view.CurrentNewspaper);
            _view.UpdateNewspaper += () => UpdateNewspaper(_view.CurrentNewspaper);
            _view.DeleteArticleNewspaper += () => DeleteArticleNewspaper(_view.GetCurrentArticleNewspaper.Id, _view.GetNewspaperToDelete.Newspaper.Id);
            _view.GetFreeNewspaperArticles += () => GetFreeNewspaperArticles();
            _view.Show();
        }

        private void RemoveCurrentNewspaper(NewspaperModel currentNewspaper)
        {
            _newspaperService.RemoveNewspaper(currentNewspaper);
            _view.LibraryNewspapers = _newspaperService.GetNewspapers;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }

        public void Run()
        {
            _view.Show();

        }
        public void Close()
        {
            _view.Close();

        }

        private void UpdateNewspaper(NewspaperModel newspaper)
        {
            _newspaperService.UpdateNewspaper(newspaper);
            _view.LibraryNewspapers = _newspaperService.GetNewspapers;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }
        private void GetFreeNewspaperArticles()
        {
            _view.GetAllArticle = _articleService.GetAllFreeNewspaperArticles();
        }
        private void AddNewspaper(NewspaperViewModel newspaper)
        {
            _newspaperService.AddNewspaper(newspaper);
            _view.LibraryNewspapers = _newspaperService.GetNewspapers;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }

        private void DeleteArticleNewspaper(int articleId, int newspaperId)
        {
            _newspaperService.DeleteArticleNewspaper(articleId, newspaperId);
            _view.LibraryNewspapers = _newspaperService.GetNewspapers;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }
        private void FindNewspapers(NewspaperViewModel newspaper)
        {
            _view.LibraryNewspapers = _newspaperService.FindNewspapers(newspaper);
            _view.GetAllArticle = _articleService.GetAllArticles;
        }
    }
}
