﻿using Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Model;
using Library.Model.ViewModel;
using Library.Services;

namespace Library.Presentation
{
    public class AddNewArticlePresentation 
    {
        private IAddNewArticlePresentation _view;
        private ArticleService _articleService;
        private AuthorService _authorService;
        public AddNewArticlePresentation(IAddNewArticlePresentation view)
        {
            _view = view;
            _articleService = new ArticleService();
            _authorService = new AuthorService();
            _view.FindArticle += () => FindArticles();
            _view.AddArticle += () => AddArticle(_view.GetArticleToAdd);
            _view.AddAuthor += () => AddAuthor(_view.GetAuthorToAdd);
        }
        

        public void Run()
        {
            _view.Show();
        }

        private void AddArticle(ArticleViewModel article)
        {
            _articleService.AddArticle(article);
        }
        private void AddAuthor(AuthorModel author)
        {
            _authorService.CreateAuthor(author);
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }

        private void FindArticles()
        {
            _view.GetAllAuthors = _authorService.GetAllAuthors();
        }
    }
}
