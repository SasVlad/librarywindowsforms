﻿using Library.Interfaces;
using Library.Model;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class LibraryBookPresenter
    {
        private ILibraryBookView _view;
        private BookService _bookService;
        private AuthorService _authorService;
        public LibraryBookPresenter(ILibraryBookView view)
        {
            _view = view;
            _bookService = new BookService();
            _authorService = new AuthorService();
            _view.FindBook += () => FindBooks(_view.GetBook);
            _view.RemoveBook += () => RemoveCurrentBook(_view.CurrentBook);
            _view.UpdateBook += () => UpdateBook(_view.CurrentBook);
            _view.DeleteAuthorBook += () => DeleteAuthorBook(_view.GetCurrentAuthorBook.Id,_view.GetBookToDelete.Book.Id);
            _view.Show();
        }

        private void RemoveCurrentBook(BookModel currentBook)
        {
            _bookService.RemoveBook(currentBook);
           _view.LibraryBooks = _bookService.GetBooks;
            _view.GetAllAuthor = _authorService.GetAllAuthors();
        }

        public void Run()
        {
            _view.Show();
        }
        public void CloseWindow()
        {
            _view.Close();
        }
        private void UpdateBook(BookModel book)
        {
            _bookService.UpdateBook(book);
                _view.LibraryBooks = _bookService.GetBooks;
            _view.GetAllAuthor = _authorService.GetAllAuthors();
        }

        private void DeleteAuthorBook(int authorId, int bookId)
        {
            _bookService.DeleteAuthorBook(authorId, bookId);
            _view.LibraryBooks = _bookService.GetBooks;
        }
        private void FindBooks(BookViewModel book)
        {
            _view.LibraryBooks = _bookService.FindBooks(book);
            _view.GetAllAuthor = _authorService.GetAllAuthors();
        }
    }
}
