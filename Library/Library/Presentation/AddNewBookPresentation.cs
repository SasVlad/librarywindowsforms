﻿using Library.Interfaces;
using Library.Model;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class AddNewBookPresentation
    {
        private IAddNewBookPresentation _view;
        private BookService _bookService;
        private AuthorService _authorService;
        public AddNewBookPresentation(IAddNewBookPresentation view)
        {
            _view = view;
            _bookService = new BookService();
            _authorService = new AuthorService();
            _view.FindBook += () => FindBooks();
            _view.AddBook += () => AddBook(_view.GetBookToAdd);
            _view.AddAuthor += () => AddAuthor(_view.GetAuthorToAdd);
        }

        public void Run()
        {
            _view.Show();
        }
        public void CloseWindow()
        {
            _view.Close();
        }


        private void AddBook(BookViewModel book)
        {
            _bookService.AddBook(book);
        }
        private void AddAuthor(AuthorModel author)
        {
            _authorService.CreateAuthor(author);
            _view.GetAllAuthor = _authorService.GetAllAuthors();
        }

        private void FindBooks()
        {
            _view.GetAllAuthor = _authorService.GetAllAuthors();
        }
    }
}
