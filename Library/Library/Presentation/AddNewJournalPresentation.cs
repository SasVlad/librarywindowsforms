﻿using Library.Interfaces;
using Library.Model.ViewModel;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class AddNewJournalPresentation
    {
        private IAddNewJournalPresentation _view;
        private JournalService _journalService;
        private ArticleService _articleService;
        public AddNewJournalPresentation(IAddNewJournalPresentation view)
        {
            _view = view;
            _journalService = new JournalService();
            _articleService = new ArticleService();

            _view.AddJournal += () => AddJournal(_view.GetJournalToAdd);
            _view.GetFreeArticles += () => GetFreeArticles();
        }



        public void Run()
        {
            _view.Show();
        }

        private void GetFreeArticles()
        {
            _view.GetAllArticle = _articleService.GetAllFreeJournalArticles();
        }
        private void AddJournal(JournalViewModel journal)
        {
            _journalService.AddJournal(journal);
            _view.GetAllArticle = _articleService.GetAllArticles;
        }

    }
}
