﻿using Library.Interfaces;
using Library.Model;
using Library.Model.ViewModel;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Presentation
{
    public class LibraryJournalPresenter
    {
        private ILibraryJournalView _view;
        private JournalService _journalService;
        private ArticleService _articleService;
        public LibraryJournalPresenter(ILibraryJournalView view)
        {
            _view = view;
            _journalService = new JournalService();
            _articleService = new ArticleService();
            _view.FindJournal += () => FindJournals(_view.GetJournal);
            _view.RemoveJournal += () => RemoveCurrentJournal(_view.CurrentJournal);
            _view.UpdateJournal += () => UpdateJournal(_view.CurrentJournal);
            _view.DeleteArticleJournal += () => DeleteArticleJournal(_view.GetCurrentArticleJournal.Id, _view.GetJournalToDelete.Journal.Id);
            _view.Show();
        }

        private void RemoveCurrentJournal(JournalModel currentJournal)
        {
            _journalService.RemoveJournal(currentJournal);
            _view.LibraryJournals = _journalService.GetJournals;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }

        public void Run()
        {
            _view.Show();
        }
        private void UpdateJournal(JournalModel journal)
        {
            _journalService.UpdateJournal(journal);
            _view.LibraryJournals = _journalService.GetJournals;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }

        private void DeleteArticleJournal(int articleId, int journalId)
        {
            _journalService.DeleteArticleJournal(articleId, journalId);
            _view.LibraryJournals = _journalService.GetJournals;
            _view.GetAllArticle = _articleService.GetAllArticles;
        }
        private void FindJournals(JournalViewModel journal)
        {
            _view.LibraryJournals = _journalService.FindJournals(journal);
            _view.GetAllArticle = _articleService.GetAllArticles;
        }
    }
}
