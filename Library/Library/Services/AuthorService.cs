﻿using Library.Model;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class AuthorService
    {
        private AuthorRepository _authorRepository = new AuthorRepository();
        public List<AuthorModel> GetAllAuthors()
        {
            return _authorRepository.GetAllDistinctAuthors();
        }
        public void CreateAuthor(AuthorModel author)
        {
            _authorRepository.CreateAuthor(author);
        }
    }
}
