﻿using Library.Model;
using Library.Model.ViewModel;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class NewspaperService
    {
        private NewspaperRepository _newspaperRepository;
        public NewspaperService()
        {
            _newspaperRepository = new NewspaperRepository();
        }
        public List<NewspaperViewModel> GetNewspapers
        {
            get
            {
                return _newspaperRepository.GetNewspapers();
            }
        }

        public void RemoveNewspaper(NewspaperModel currentNewspaper)
        {
            if (currentNewspaper != null)
            {
                _newspaperRepository.DeleteNewspaperById(currentNewspaper.Id);
            }
        }
        public void UpdateNewspaper(NewspaperModel newspaper)
        {
            if (newspaper != null)
            {
                _newspaperRepository.UpdateNewspaper(newspaper);
            }
        }
        public void AddNewspaper(NewspaperViewModel newspaper)
        {
            if (newspaper != null)
            {
                _newspaperRepository.CreateNewspaper(newspaper);
            }
        }
        public void DeleteArticleNewspaper(int articleId, int newspaperId)
        {
            if (newspaperId != 0 && articleId != 0)
            {
                _newspaperRepository.DeleteArticleNewspaperById(articleId, newspaperId);
            }
        }
        public List<NewspaperViewModel> FindNewspapers(NewspaperViewModel newspaper)
        {
            if (newspaper != null)
            {
                if (newspaper.Newspaper.Id != 0)
                {
                    return new List<NewspaperViewModel> { _newspaperRepository.GetNewspaperById(newspaper.Newspaper.Id) };
                }
                if (newspaper.Newspaper.Name != "")
                {
                    return new List<NewspaperViewModel> { _newspaperRepository.GetNewspaperByNewspaperName(newspaper.Newspaper.Name) };
                }

                if (newspaper.Articles.Count != 0)
                {
                    var _listNewspapers = _newspaperRepository.GetNewspaperByNewspaperArticles(newspaper.Articles);

                    return _listNewspapers;
                }
            }
            return _newspaperRepository.GetNewspapers();
        }
    }
}
