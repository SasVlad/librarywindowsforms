﻿using Library.Model;
using Library.Model.ViewModel;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class JournalService
    {
        private JournalRepository _journalRepository;
        public JournalService()
        {
            _journalRepository = new JournalRepository();
        }
        public List<JournalViewModel> GetJournals
        {
            get
            {
                return _journalRepository.GetJournals();
            }
        }

        public void RemoveJournal(JournalModel currentJournal)
        {
            if (currentJournal != null)
            {
                _journalRepository.DeleteJournalById(currentJournal.Id);
            }
        }
        public void UpdateJournal(JournalModel journal)
        {
            if (journal != null)
            {
                _journalRepository.UpdateJournal(journal);
            }
        }
        public void AddJournal(JournalViewModel journal)
        {
            if (journal != null)
            {
                _journalRepository.CreateJournal(journal);
            }
        }
        public void DeleteArticleJournal(int articleId, int journalId)
        {
            if (journalId != 0 && articleId != 0)
            {
                _journalRepository.DeleteArticleJournalById(articleId, journalId);
            }
        }
        public List<JournalViewModel> FindJournals(JournalViewModel journal)
        {
            if (journal != null)
            {
                if (journal.Journal.Id != 0)
                {
                    return new List<JournalViewModel> { _journalRepository.GetJournalById(journal.Journal.Id) };
                }
                if (journal.Journal.Name != "")
                {
                    return new List<JournalViewModel> { _journalRepository.GetJournalByJournalName(journal.Journal.Name) };
                }

                if (journal.Articles.Count != 0)
                {
                    var _listJournals = _journalRepository.GetJournalByJournalArticles(journal.Articles);

                    return _listJournals;
                }
            }
            return _journalRepository.GetJournals();
        }
    }
}
