﻿using Library.Model;
using Library.Model.ViewModel;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class ArticleService
    {
        private ArticleRepository _articleRepository;
        public ArticleService()
        {
            _articleRepository = new ArticleRepository();
        }
        public List<ArticleViewModel> GetArticles
        {
            get
            {
                return _articleRepository.GetArticles();
            }
        }
        public List<ArticleModel> GetAllFreeJournalArticles()
        {
            return _articleRepository.GetFreeJournalArticles();
        }
        public List<ArticleModel> GetAllFreeNewspaperArticles()
        {
            return _articleRepository.GetFreeNewspaperArticles();
        }
        public List<ArticleModel> GetAllArticles
        {
            get
            {
                return _articleRepository.GetAllArticles();
            }
        }
        public void RemoveArticle(ArticleModel currentArticle)
        {
            if (currentArticle != null)
            {
                _articleRepository.DeleteArticleById(currentArticle.Id);
            }
        }
        public void UpdateArticle(ArticleModel article)
        {
            if (article != null)
            {
                _articleRepository.UpdateArticle(article);
            }
        }
        public void AddArticle(ArticleViewModel article)
        {
            if (article != null)
            {
                _articleRepository.CreateArticle(article);
            }
        }
        public void DeleteAuthorArticle(int authorId, int articleId)
        {
            if (articleId != 0 && authorId != 0)
            {
                _articleRepository.DeleteAuthorArticleById(authorId, articleId);
            }
        }
        public List<ArticleViewModel> FindArticles(ArticleViewModel article)
        {
            if (article != null)
            {
                if (article.Article.Id != 0)
                {
                    return new List<ArticleViewModel> { _articleRepository.GetArticleById(article.Article.Id) };
                }
                if (article.Article.Title != "")
                {
                    return new List<ArticleViewModel> { _articleRepository.GetArticleByArticleTitle(article.Article.Title) };
                }

                if (article.Authors.Count != 0)
                {
                    var _listBooks = _articleRepository.GetArticleByArticleAuthors(article.Authors);

                    return _listBooks;
                }
            }
            return _articleRepository.GetArticles();
        }
    }
}
