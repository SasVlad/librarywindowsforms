﻿using Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class BookService
    {
        private BookRepositoryDb _bookRepository;
        public BookService()
        {
            _bookRepository = new BookRepositoryDb();
        }
        public List<BookViewModel> GetBooks
        {
            get
            {
                return _bookRepository.GetBooks();
            }
        }

        public void RemoveBook(BookModel currentBook)
        {
            if (currentBook != null)
            {
                _bookRepository.DeleteBookById(currentBook.Id);               
            }
        }
        public void UpdateBook(BookModel book)
        {
            if (book != null)
            {
                _bookRepository.UpdateBook(book);
            }
        }
        public void AddBook(BookViewModel book)
        {
            if (book != null)
            {
                _bookRepository.CreateBook(book);
            }
        }
        public void DeleteAuthorBook(int authorId,int bookId)
        {
            if (bookId != 0 && authorId!=0)
            {
                _bookRepository.DeleteAuthorBookById(authorId,bookId);
            }
        }
        public List<BookViewModel> FindBooks(BookViewModel book)
        {
            if (book != null)
            {
                if (book.Book.Id != 0)
                {
                    return new List<BookViewModel> { _bookRepository.GetBookById(book.Book.Id) };
                }
                if (book.Book.Name != "")
                {
                    return new List<BookViewModel> { _bookRepository.GetBookByBookName(book.Book.Name) };
                }

                if (book.Authors.Count != 0)
                {
                    var _listBooks = _bookRepository.GetBookByBookAuthors(book.Authors);
                    //foreach (var _book in _bookRepository.GetBookByBookAuthors(book.Authors))
                    //{
                    //    _listBooks.Add(new BookViewModel { Book = _book });
                    //}

                    return _listBooks;
                }
            }
            return _bookRepository.GetBooks();
        }

        }
}
