﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Library.Repositories;
using Library.Model;
using Library.Model.ViewModel;

namespace Library
{
    public static class InitialDatabase
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static string GetConnectionString()
        {            
                if (connectionString== "")
                {
                    connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                }
                return connectionString;
        }
        public static void InitDatabaseTables()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                DropTables(db);
                CreateTables(db);
                Seed();
            }

        }
        
        public static void Seed()
        {
            BookRepositoryDb _bookRepository = new BookRepositoryDb();
            AuthorRepository _authorRepository = new AuthorRepository();
            ArticleRepository _articleRepository = new ArticleRepository();
            JournalRepository _journalRepository = new JournalRepository();
            NewspaperRepository _newspaperRepository = new NewspaperRepository();
            
            
            var _author1 = new AuthorModel { FirstName = "Adam", LastName = "Freeman" };
            var _author2 = new AuthorModel { FirstName = "Andrey", LastName = "Troyalson" };
            var _author3 = new AuthorModel { FirstName = "Jefry", LastName = "Rihter" };

            var book1 = new BookModel { Name = "Asp.Net Mvc", YearOfBook = 2015, Publisher = "Apress", Edition = 25000, CountPages = 1024, Cover = "Hard cover" };
            var book2 = new BookModel { Name = "Asp.Net WebApi2", YearOfBook = 2012, Publisher = "Apress", Edition = 23000, CountPages = 965, Cover = "Soft cover" };
            var book3 = new BookModel { Name = "C# via .net", YearOfBook = 2008, Publisher = "Apress", Edition = 23000, CountPages = 1000, Cover = "Soft cover" };

            var _author4 = new AuthorModel { FirstName = "Mikle", LastName = "Larman" };
            var _author5 = new AuthorModel { FirstName = "John", LastName = "Kipr" };
            var _author6 = new AuthorModel { FirstName = "Kira", LastName = "Munhen" };

            var article1 = new ArticleModel { Title = "C# is a best lenguage", Image = "C:/dfsdfs/", Body = "Body{C# is a best lenguage}" };
            var article2 = new ArticleModel { Title = "Kharkiv is a best city", Image = "D:/dfsdfs/", Body = "Body{Kharkiv is a best city}" };
            var article3 = new ArticleModel { Title = "Hello from Kharkiv", Image = "F:/dfsdfs/", Body = "Body{Hello from Kharkiv}" };

            var newspaper1 = new NewspaperModel { Name = "Asp.Net Mvc", MainTheme = "sss", Publisher = "Apress", TypeNewspaper = "MvcNewspaper", CountPages = 1547, Edition = 1541 };
            var newspaper2 = new NewspaperModel { Name = "Asp.Net WebApi2", MainTheme = "aaa", Publisher = "Apress", TypeNewspaper = "WebApi2Newspaper", CountPages = 2547, Edition = 2541 };
            var newspaper3 = new NewspaperModel { Name = "C# via .net", MainTheme = "bbb", Publisher = "Apress", TypeNewspaper = "netNewspaper", CountPages = 3547, Edition = 3541 };

            var journal1 = new JournalModel { Name = "Asp.Net Mvc", Genre = "sss", Publisher = "Apress", Type = "MvcJournal", CountPages = 1547, Cover = "cover1", Edition = 1541 };
            var journal2 = new JournalModel { Name = "Asp.Net WebApi2", Genre = "aaa", Publisher = "Apress", Type = "WebApi2Journal", CountPages = 2547, Cover = "cover2", Edition = 2541 };
            var journal3 = new JournalModel { Name = "C# via .net", Genre = "bbb", Publisher = "Apress", Type = "netJournal", CountPages = 3547, Cover = "cover3", Edition = 3541 };

            
                var _author1Id = _authorRepository.CreateAuthor(_author1);
                var _author2Id = _authorRepository.CreateAuthor(_author2);
                var _author3Id = _authorRepository.CreateAuthor(_author3);

                _author1.Id = _author1Id;
                _author2.Id = _author2Id;
                _author3.Id = _author3Id;


            _bookRepository.CreateBook(new BookViewModel
            {
                Book = book1,
                Authors = new List<AuthorModel> { _author1, _author2 }
            });
            _bookRepository.CreateBook(new BookViewModel
                {
                    Book = book2,
                    Authors = new List<AuthorModel> { _author2, _author3 }
                });
            _bookRepository.CreateBook(new BookViewModel
                {
                    Book = book3,
                    Authors = new List<AuthorModel> { _author3 }
                });
                //-----Article

                
                    var _author4Id = _authorRepository.CreateAuthor(_author4);
                    var _author5Id = _authorRepository.CreateAuthor(_author5);
                    var _author6Id = _authorRepository.CreateAuthor(_author6);

                    _author4.Id = _author4Id;
                    _author5.Id = _author5Id;
                    _author6.Id = _author6Id;


            _articleRepository.CreateArticle(new ArticleViewModel
                {
                    Article = article1,
                    Authors = new List<AuthorModel> { _author4, _author5 }
                });
            _articleRepository.CreateArticle(new ArticleViewModel
                {
                    Article = article2,
                    Authors = new List<AuthorModel> { _author5, _author6 }
                });
            _articleRepository.CreateArticle(new ArticleViewModel
                {
                    Article = article3,
                    Authors = new List<AuthorModel> { _author6 }
                });

                //--------------Newspaper


                
                    var _newspaper1Id = _newspaperRepository.CreateNewspaper(new NewspaperViewModel { Newspaper = newspaper1, Articles = new List<ArticleModel> { new ArticleModel { Id = 1 }, new ArticleModel { Id = 2 } } });
                    var _newspaper2Id = _newspaperRepository.CreateNewspaper(new NewspaperViewModel { Newspaper = newspaper2, Articles = new List<ArticleModel> { new ArticleModel { Id = 3 } } });
                    var _newspaper3Id = _newspaperRepository.CreateNewspaper(new NewspaperViewModel { Newspaper = newspaper3, Articles = new List<ArticleModel> { } });
                
                //-------Journal


                
                    var _journal1Id = _journalRepository.CreateJournal(new JournalViewModel { Journal = journal1, Articles = new List<ArticleModel> { new ArticleModel { Id = 1 }, new ArticleModel { Id = 2 } } });
                    var _journal2Id = _journalRepository.CreateJournal(new JournalViewModel { Journal = journal2, Articles = new List<ArticleModel> { new ArticleModel { Id = 3 } } });
                    var _journal3Id = _journalRepository.CreateJournal(new JournalViewModel { Journal = journal3, Articles = new List<ArticleModel> { } });
                
            
        }
        public static void DropTables(IDbConnection db)
        {
                db.Execute("Drop table AuthorPolygraphies");
                db.Execute("Drop table Books");
                db.Execute("Drop table Articles");
                db.Execute("Drop table Authors");
                db.Execute("Drop table Journals");
                db.Execute("Drop table Newspapers");
                
                
            
        }
        public static void CreateTables(IDbConnection db)
        {
            
            db.Execute(@"Create table  Books(  
                            Id integer primary key IDENTITY(1,1) NOT NULL ,
                            Name varchar(100) not null,
                            CountPages integer not null,
                            Edition integer not null,
                            Publisher varchar(100) not null,
                            Cover varchar(100) not null,
                            YearOfBook integer not null)");

            db.Execute(@"Create table Authors( 
                            Id integer primary key IDENTITY(1,1) NOT NULL,
                            FirstName varchar(100) not null,
                            LastName varchar(100) not null)");

            db.Execute(@"Create table Journals(
                            Id integer primary key IDENTITY(1,1) NOT NULL,
                            Name varchar(100) not null,
                            CountPages integer not null,
                            Edition integer not null,
                            Publisher varchar(100) not null,
                            Cover varchar(100) not null,
                            Type varchar(100) not null,
                            Genre varchar(100) not null)");

            db.Execute(@"Create table Newspapers(
                            Id integer primary key IDENTITY(1,1) NOT NULL,
                            Name varchar(100) not null,
                            CountPages integer not null,
                            Edition integer not null,
                            Publisher varchar(100) not null,
                            TypeNewspaper varchar(100) not null,
                            MainTheme varchar(100) not null)");

            db.Execute(@"Create table Articles( 
                            Id integer primary key IDENTITY(1,1) NOT NULL,
                            Title varchar(100) not null,
                            Image varchar(100) not null,
                            Body varchar(100) not null,
                            NewspaperId_FK integer foreign key references Newspapers(Id),
                            JournalId_FK integer foreign key references Journals(Id))");

            db.Execute(@"Create table AuthorPolygraphies( 
                            Id integer primary key IDENTITY(1,1) NOT NULL,
                            BookId_FK integer foreign key references  Books(Id),
                            ArticleId_FK integer foreign key references Articles(Id),
                            AuthorId_FK integer foreign key references Authors(Id) not null)");

                
         }
        
    }
}
