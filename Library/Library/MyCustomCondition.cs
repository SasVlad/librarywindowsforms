﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public static class MyCustomCondition
    {
        public static void ChooseCondition(bool condition, Action succesCond, string errorMessage)
        {

            if (condition)
            {
                succesCond();
            }
            if (!condition)
            {
                MessageBox.Show(errorMessage);
            }

        }
    }
}
