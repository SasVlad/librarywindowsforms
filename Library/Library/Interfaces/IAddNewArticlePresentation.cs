﻿using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface IAddNewArticlePresentation : IView
    {
        List<AuthorModel> GetAllAuthors { get; set; }
        ArticleViewModel GetArticleToAdd { get; }
        AuthorModel GetAuthorToAdd { get; }
        event Action AddAuthor;
        event Action AddArticle;
        event Action FindArticle;
    }
}
