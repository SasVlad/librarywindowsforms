﻿using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface ILibraryJournalView : IView
    {
        JournalViewModel GetJournal { get; }
        List<JournalViewModel> LibraryJournals { get; set; }
        List<ArticleModel> GetAllArticle { get; set; }
        JournalViewModel GetJournalToDelete { get; }
        ArticleModel GetArticleToAdd { get; }
        JournalModel CurrentJournal { get; }
        ArticleModel GetCurrentArticleJournal { get; }
        event Action RemoveJournal;
        event Action FindJournal;
        event Action UpdateJournal;
        event Action DeleteArticleJournal;
    }
}
