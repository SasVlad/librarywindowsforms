﻿using Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface ILibraryBookView:IView
    {
        BookViewModel GetBook { get; }
        List<BookViewModel> LibraryBooks { get; set; }
        BookViewModel GetBookToDelete { get; }
        List<AuthorModel> GetAllAuthor { get; set; }
        BookModel CurrentBook { get; }
        AuthorModel GetCurrentAuthorBook { get; }
        event Action RemoveBook;      
        event Action FindBook;
        event Action UpdateBook;
        event Action DeleteAuthorBook;
    }
}
