﻿using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface ILibraryArticleView : IView
    {
        ArticleViewModel GetArticle { get; }
        List<ArticleViewModel> LibraryArticles { get; set; }
        List<AuthorModel> GetAllAuthors { get; set; }
        AuthorModel GetAuthorToAdd { get; }
        ArticleModel CurrentArticle { get; }
        ArticleViewModel GetArticleToDelete { get; }
        AuthorModel GetCurrentAuthorArticle { get; }
        event Action RemoveArticle;
        event Action FindArticle;
        event Action UpdateArticle;
        event Action DeleteAuthorArticle;
    }
}
