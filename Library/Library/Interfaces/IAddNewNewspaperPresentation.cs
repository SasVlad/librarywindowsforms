﻿using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface IAddNewNewspaperPresentation:IView
    {
        List<ArticleModel> GetAllArticle { get; set; }
        NewspaperViewModel GetNewspaperToAdd { get; }
        ArticleModel GetArticleToAdd { get; }
        event Action AddNewspaper;
        event Action GetFreeNewspaperArticles;
    }
}
