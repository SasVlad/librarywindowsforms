﻿using Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface IAddNewBookPresentation:IView
    {
        List<AuthorModel> GetAllAuthor { get; set; }
        BookViewModel GetBookToAdd { get; }
        AuthorModel GetAuthorToAdd { get; }
        event Action AddAuthor;
        event Action AddBook;
        event Action FindBook;
    }
}
