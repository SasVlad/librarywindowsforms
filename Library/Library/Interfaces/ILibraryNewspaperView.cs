﻿using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface ILibraryNewspaperView : IView
    {
        NewspaperViewModel GetNewspaper { get; }
        List<NewspaperViewModel> LibraryNewspapers { get; set; }
        List<ArticleModel> GetAllArticle { get; set; }
        NewspaperViewModel GetNewspaperToDelete { get; }
        ArticleModel GetArticleToAdd { get; }
        NewspaperModel CurrentNewspaper { get; }
        ArticleModel GetCurrentArticleNewspaper { get; }
        event Action RemoveNewspaper;
        event Action FindNewspaper;
        event Action UpdateNewspaper;
        event Action DeleteArticleNewspaper;
        event Action GetFreeNewspaperArticles;
    }
}
