﻿using Library.Model;
using Library.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Interfaces
{
    public interface IAddNewJournalPresentation:IView
    {
        JournalViewModel GetJournalToAdd { get; }
        List<ArticleModel> GetAllArticle { get; set; }
        event Action AddJournal;
        event Action GetFreeArticles;
    }
}
